@file:Suppress("PrivatePropertyName", "SpellCheckingInspection", "LocalVariableName", "LogConditional", "unused")
package com.swoopin.android.application

// Android stuff
import android.content.Intent
import android.app.Application

// Swoopin stuff
import com.swoopin.android.utility.*
import com.swoopin.android.network.SwoopinAPI
import com.swoopin.android.activity.LaunchActivity

// Custom Application class for MLBTS
class SwoopinApplication : Application()
{
    // Static fields
    companion object {
        const val LOGTAG = "SwoopinApplication"
        lateinit var instance: SwoopinApplication
            private set

        fun settings(): Settings {
            return instance.getSettings()
        }
    }

    val m_settings = Settings(this)

    // Class initializer
    init {
        instance = this
    }

    override fun onCreate()
    {
        // Calculate memory metrics
        super.onCreate()
        Utility.logMemoryMetrics(applicationContext)
    }

    private fun getSettings(): Settings {
        return instance.m_settings
    }

    // Log user out
    fun logOutUser()
    {
        // Reset user profile
        SwoopinAPI.profileInfo.reset()
        settings().clear()
    }

    // Low memory handler
    override fun onLowMemory()
    {
        super.onLowMemory()
        Utility.logDebug(LOGTAG,"onLowMemory called. This is getting serious...")

        // Trace memory metrics
        Utility.logMemoryMetrics(applicationContext)
    }

    // Trim memory handler
    override fun onTrimMemory(level: Int)
    {
        super.onTrimMemory(level)

        // Determine which lifecycle or system event was raised
        when (level)
        {
            TRIM_MEMORY_UI_HIDDEN ->
                Utility.logDebug(LOGTAG,"Our UI has been moved to the background, can be freed.")
            TRIM_MEMORY_RUNNING_MODERATE, TRIM_MEMORY_RUNNING_LOW, TRIM_MEMORY_RUNNING_CRITICAL ->
                Utility.logDebug(LOGTAG,"Running low on memory, Level: $level")
            TRIM_MEMORY_BACKGROUND, TRIM_MEMORY_MODERATE, TRIM_MEMORY_COMPLETE ->
                Utility.logDebug(LOGTAG,"Running low on Memory, App is on LRU list. Level: $level")
            else ->
                Utility.logDebug(LOGTAG,"Unrecognized Memory Message. Level: $level")
        }

        // Trace memory metrics
        Utility.logMemoryMetrics(applicationContext)
    }
}
