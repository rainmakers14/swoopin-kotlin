package com.swoopin.android.activity

// Android Stuff
import androidx.activity.result.contract.ActivityResultContracts
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ItemTouchHelper
import android.text.method.ScrollingMovementMethod
import android.view.inputmethod.InputMethodManager
import androidx.recyclerview.widget.RecyclerView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AlertDialog
import kotlinx.coroutines.runBlocking
import android.graphics.ImageDecoder
import android.provider.MediaStore
import android.widget.ImageButton
import android.content.Context
import android.graphics.Bitmap
import android.content.Intent
import android.app.Activity
import android.widget.Toast
import android.os.Bundle
import android.view.View
import android.os.Build
import android.net.Uri

// Swoopin Stuff
import com.swoopin.android.application.SwoopinApplication
import com.swoopin.android.databinding.ViewEventBinding
import com.swoopin.android.adapter.CommentListAdapter
import com.swoopin.android.utility.Permissions
import com.swoopin.android.utility.Settings
import com.swoopin.android.utility.Utility
import com.swoopin.android.network.*
import com.swoopin.android.R
import io.ktor.util.*

// ---------------------------------------------------------------------------------------
//  ViewEventActivity - Activity to view a single event
// ---------------------------------------------------------------------------------------
@Suppress("UNUSED_PARAMETER")
class ViewEventActivity : AppCompatActivity(), Events.EventCallback, Users.UserCallback
{
    // Values needed to configure this event viewer
    companion object {
        const val EXTRA_EVENT_ID = "EVENT_ID"
        // Track current position of comment list
        var currentScrollPosition = 0
    }

    // Permissions manager
    private val permissions = Permissions(this)

    // Activity resources
    private lateinit var binding: ViewEventBinding
    private var eventId: Int = 0

    // Comment list adapter
    private lateinit var commentAdapter: CommentListAdapter

    // Comment bitmap
    var commentBitmap: Bitmap? = null

    // Intent filters
    private val REQUEST_IMAGE_CAPTURE = 1

    // True while refreshing comment list
    private var refreshing = false

    // Keep track of following status so we don't have to refresh events when the button is pressed
    private var swoopedIn = false

    // ---------------------------------------------------------------------------------------
    //  onCreate - Initialize activity
    // ---------------------------------------------------------------------------------------
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)

        // Hide top bar (how to do this in XML instead?
        supportActionBar?.hide()

        // Bind layout resources
        binding = ViewEventBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Initialize comment list adapter
        commentAdapter = CommentListAdapter(this)
        binding.comments.adapter = commentAdapter
        binding.comments.layoutManager = LinearLayoutManager(this)
        binding.comments.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        val itemTouchHelper = ItemTouchHelper(SwipeToDeleteCallback(commentAdapter, (ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT)))
        itemTouchHelper.attachToRecyclerView(binding.comments)

        // Get the event to be displayed
        eventId = intent.getIntExtra(EXTRA_EVENT_ID, 0)
        loadEvent(eventId)

        // Remember current scroll position
        binding.comments.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int)
            {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    val layoutManager: LinearLayoutManager = recyclerView.layoutManager as LinearLayoutManager
                    currentScrollPosition = layoutManager.findLastVisibleItemPosition()
                }
            }
        })
    }

    // ---------------------------------------------------------------------------------------
    //  onPause - Activity is being backgrounded
    // ---------------------------------------------------------------------------------------
    override fun onPause()
    {
        Events.unsubscribeRefreshEvents(this)
        Users.unsubscribeRefreshUsers(this)
        super.onPause()
    }

    // ---------------------------------------------------------------------------------------
    //  onResume - Activity is being foregrounded
    // ---------------------------------------------------------------------------------------
    override fun onResume()
    {
        super.onResume()
        Events.subscribeRefreshEvents(this)
        Users.subscribeRefreshUsers(this)
    }

    // ---------------------------------------------------------------------------------------
    //  onBackPressed - Clean up when exiting
    // ---------------------------------------------------------------------------------------
    override fun onBackPressed()
    {
        // Clean up when exiting
        currentScrollPosition = 0
        super.onBackPressed()
    }

    // ---------------------------------------------------------------------------------------
    //  loadEvent - Load data for specified event into the UI
    // ---------------------------------------------------------------------------------------
    private fun loadEvent(evId: Int)
    {
        // Set each field
        with (binding) {
            eventTitle.text = Events.getTitle(evId)
            eventDesc.text = Events.getDesc(evId)
            eventDesc.movementMethod = ScrollingMovementMethod()
            SwoopinAPI.loadImage(Events.getCreatorImage(evId), creatorIcon)
            creatorName.text = Events.getCreatorName(evId)
            eventLocation.text = com.swoopin.android.network.Events.getLocation(evId)
            goingButton.text = String.format(getString(R.string.fmt_going_button), Events.numAttending(evId))

            // Event time
            val start = Events.formatStartTime(evId)
            val end = Events.formatEndTime(evId)
            eventStart.text = "$start to $end"

            // Set initial SwoopIn button text
            val userId = SwoopinApplication.settings().getIntegerValue(Settings.PREFKEY_USERID, 0)
            swoopedIn = Events.isAttending(userId, evId)
            setSwoopinButton()

            // Set the list of comments
            commentAdapter.setList(Events.getComments(evId))

            // Set current scroll position
            binding.comments.layoutManager?.scrollToPosition(currentScrollPosition)
        }
    }

    // ---------------------------------------------------------------------------------------
    //  SwipeToDeleteCallback - Holder for all the view items to be filled in by the data
    // ---------------------------------------------------------------------------------------
    inner class SwipeToDeleteCallback(private val mAdapter: CommentListAdapter, swipeDirs: Int):
        ItemTouchHelper.SimpleCallback(0, (ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT))
    {
        override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
            return true
        }

        override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
            val position = viewHolder.adapterPosition
            if (direction == ItemTouchHelper.LEFT)
                deleteComment(mAdapter.getCommentId(position))
        }
    }

    // ---------------------------------------------------------------------------------------
    //  deleteComment - Delete item at specified index
    // ---------------------------------------------------------------------------------------
    private fun deleteComment(commentId: Int)
    {
        // Really should disable deletion swipes until server returns!

        // Send comment to server
        refreshing = true
        runBlocking {
            SwoopinAPI.deleteComment(eventId, commentId) { result ->
                // Async results arrived
                refreshing = false
                if (result != SwoopinAPI.API_SUCCESS) {
                    // Log error message
                    val errStr = "Failed to delete comment: $result"
                    Utility.logDebug(errStr)
                    Toast.makeText(this@ViewEventActivity, result, Toast.LENGTH_LONG).show()
                    loadEvent(eventId)
                }
                else {
                    // Reload event with updated comments
                    loadEvent(eventId)
                }
            }
        }
    }

    // ---------------------------------------------------------------------------------------
    //  setSwoopinButton - Toggle Swoopin button
    // ---------------------------------------------------------------------------------------
    private fun setSwoopinButton()
    {
        if (swoopedIn)
            binding.swoopinButton.text = getString(R.string.swoopout_button)
        else
            binding.swoopinButton.text = getString(R.string.swoopin_button)
    }

    // ---------------------------------------------------------------------------------------
    //  onEventsRefreshed - Get notified when the master event list is updated
    // ---------------------------------------------------------------------------------------
    override fun onEventsRefreshed()
    {
        // Reload the event
        loadEvent(eventId)
    }

    // ---------------------------------------------------------------------------------------
    //  onUsersRefreshed - Get notified when the master user list is updated
    // ---------------------------------------------------------------------------------------
    override fun onUsersRefreshed()
    {
        // Reload the event
        loadEvent(eventId)
    }

    // ---------------------------------------------------------------------------------------
    //  startRefreshing - Setup for async call (disable buttons, etc.)
    // ---------------------------------------------------------------------------------------
    private fun startRefreshing()
    {
        // Disable buttons, set flag, etc.
        binding.swoopinButton.isEnabled = false
        binding.cameraButton.isEnabled = false
        binding.commentImage.isEnabled = false
        binding.sendButton.isEnabled = false
        refreshing = true
    }

    // ---------------------------------------------------------------------------------------
    //  endRefreshing - Reset everything after async call returns
    // ---------------------------------------------------------------------------------------
    private fun endRefreshing()
    {
        // Enable buttons, etc.
        binding.swoopinButton.isEnabled = true
        binding.cameraButton.isEnabled = true
        binding.commentImage.isEnabled = true
        binding.sendButton.isEnabled = true
        refreshing = false
    }

    // ---------------------------------------------------------------------------------------
    //  toggleVotedStatus - Toggle voted status on specified comment
    // ---------------------------------------------------------------------------------------
    fun toggleVotedStatus(comment: CommentInfo, view: View)
    {
        // Determine vote status
        var voted = 1
        if (comment.userVoted > 0)
            voted = 0

        // Disable voted icon for now
        val voteIcon = view.findViewById<ImageButton>(R.id.vote_icon)
        voteIcon.isEnabled = false
        startRefreshing()

        // Vote/unvote for comment
        runBlocking {
            SwoopinAPI.voteComment(eventId, comment.id, voted) { result ->
                // Async results arrived
                endRefreshing()
                voteIcon.isEnabled = true
                if (result != SwoopinAPI.API_SUCCESS) {
                    // Log error message
                    val errStr = "Failed to change vote for comment: $result"
                    Utility.logDebug(errStr)
                }
                else {
                    // Refresh event so adapter data is correct
                    loadEvent(eventId)
                }
            }
        }
    }

    // ---------------------------------------------------------------------------------------
    //  followEvent - Send request to follow
    // ---------------------------------------------------------------------------------------
    fun followEvent(evId: Int, unfollow: Boolean)
    {
        // Request new Event data
        startRefreshing()
        runBlocking {
            SwoopinAPI.followEvent(eventId, unfollow) { result ->
                // Async results arrived
                endRefreshing()
                if (result != SwoopinAPI.API_SUCCESS) {
                    // Log error message
                    val errStr = "Failed to follow event: $result"
                    Utility.logDebug(errStr)
                }
                else {
                    // Refresh event from server so local data is correct
//                    Events.refresh()
                    swoopedIn = !unfollow
                    setSwoopinButton()
                }
            }
        }
    }

    // ---------------------------------------------------------------------------------------
    //  reportButtonClick - Report/Flag event
    // ---------------------------------------------------------------------------------------
    fun reportButtonClick(view: View)
    {
        Toast.makeText(this,"Report button not yet implemented", Toast.LENGTH_SHORT).show()

        // Do a pop-up dialog like iOS with "Yes | Cancel" buttons?
    }

    // ---------------------------------------------------------------------------------------
    //  voteButtonClick - Add or remove vote
    // ---------------------------------------------------------------------------------------
    fun voteButtonClick(view: View)
    {
        // Get tag that contains comment ID
        val voteIcon = view.findViewById<ImageButton>(R.id.vote_icon)
        val position: Int = voteIcon.tag as Int
        val comment = commentAdapter.getComment(position)

        // Send vote/unvote
        toggleVotedStatus(comment, view)
    }

    // ---------------------------------------------------------------------------------------
    //  goingButtonClick - Show list of people (friends only?) who are swooped in to the event
    // ---------------------------------------------------------------------------------------
    fun goingButtonClick(view: View)
    {
        Toast.makeText(this,"Going button not yet implemented", Toast.LENGTH_SHORT).show()

        // Show list of people who are attending
    }

    // ---------------------------------------------------------------------------------------
    //  swoopinButtonClick - Add current user to the "going" list, or send request to attend
    // ---------------------------------------------------------------------------------------
    fun swoopinButtonClick(view: View)
    {
        followEvent(eventId, swoopedIn)
    }

    // ---------------------------------------------------------------------------------------
    //  inviteButtonClick - Show lists of every type of user so they can be invited
    // ---------------------------------------------------------------------------------------
    fun inviteButtonClick(view: View)
    {
        Toast.makeText(this,"Invite button not yet implemented", Toast.LENGTH_SHORT).show()

        // Invite other users to the event
    }

    // ---------------------------------------------------------------------------------------
    //  Intent Launchers - For the new way of launching intents with results
    // ---------------------------------------------------------------------------------------

    // Intent launcher for camera
    // For some reason this must be a class-level variable
    private val cameraIntentLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                val data: Intent? = result.data
                val bitmap = data?.extras?.get("data") as Bitmap
                setCommentImage(Utility.scaleBitmapForServer(bitmap))
            }
        }

    // Intent launcher for gallery
    // For some reason this must be a class-level variable
    private val galleryIntentLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                val imageUri: Uri? = result.data?.data
                if (imageUri != null) {
                    val bitmap: Bitmap = if (Build.VERSION.SDK_INT < Build.VERSION_CODES.P)
                        MediaStore.Images.Media.getBitmap(contentResolver, imageUri)
                    else
                        ImageDecoder.decodeBitmap(ImageDecoder.createSource(contentResolver, imageUri))
                    setCommentImage(Utility.scaleBitmapForServer(bitmap))
                }
            }
        }

    // ---------------------------------------------------------------------------------------
    //  setCommentImage - Set comment image
    // ---------------------------------------------------------------------------------------
    private fun setCommentImage(srcBitmap: Bitmap)
    {
        // Clear comment image
        binding.commentImage.visibility = View.VISIBLE
        binding.commentImage.setImageBitmap(srcBitmap)
        commentBitmap = srcBitmap
    }

    // ---------------------------------------------------------------------------------------
    //  clearCommentImage - Clear comment image
    // ---------------------------------------------------------------------------------------
    private fun clearCommentImage()
    {
        // Clear comment image
        binding.commentImage.visibility = View.GONE
        binding.commentImage.setImageBitmap(null)
        commentBitmap = null
    }

    // ---------------------------------------------------------------------------------------
    //  commentImageButtonClick - Clear comment image button
    // ---------------------------------------------------------------------------------------
    fun commentImageButtonClick(view: View)
    {
        // Clear image attached to the comment
        clearCommentImage()
    }

    // ---------------------------------------------------------------------------------------
    //  cameraButtonClick - Add a picture to the comment
    // ---------------------------------------------------------------------------------------
    fun cameraButtonClick(view: View)
    {
        // https://medium.com/developer-student-clubs/android-kotlin-camera-using-gallery-ff8591c26c3e
        // https://stackoverflow.com/questions/5991319/capture-image-from-camera-and-display-in-activity
        // https://developer.android.com/training/camera/photobasics

        // Define intents
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        val galleryIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        galleryIntent.type = "image/*"

        // Check if device has camera
        var useCamera = false
        if (Utility.hasCamera(this))
        {
            // Check for camera permissions
            if (permissions.hasCameraPermission())
                useCamera = true
            else
                permissions.checkCameraPermission()
        }

        // No need to display chooser dialog if there is no camera
        if (!useCamera) {
            galleryIntentLauncher.launch(galleryIntent)
        }
        else {
            // Choose between gallery and camera
            val builder = AlertDialog.Builder(this)
            val sources = arrayOf("Use Camera", "View Gallery", "Clear Image")
            builder.setItems(sources) { _, which ->
                when (which) {
                    0 -> { cameraIntentLauncher.launch(cameraIntent) }
                    1 -> { galleryIntentLauncher.launch(galleryIntent) }
                    2 -> { clearCommentImage() }
                }
            }.create().show()
        }
    }

    // ---------------------------------------------------------------------------------------
    //  sendButtonClick - Send comment to server
    // ---------------------------------------------------------------------------------------
    @InternalAPI
    fun sendButtonClick(view: View)
    {
        // Hide keyboard
        val imm: InputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(binding.newComment.windowToken, 0)

        // Create comment
        if (commentBitmap == null)
            createComment()
        else
            createCommentWithImage()
    }

    // ---------------------------------------------------------------------------------------
    //  createCommentWithImage - Create comment with image
    // ---------------------------------------------------------------------------------------
    @InternalAPI
    fun createCommentWithImage()
    {
        // Safety first
        if (commentBitmap == null) {
            // Do this or do nothing and just return?
            createComment()
            return
        }

        // Setup for blocking call
        val newComment = binding.newComment.text.toString()
        startRefreshing()

        // Send comment to server
        runBlocking {
//            SwoopinAPI.createCommentWithImage(eventId, newComment, Utility.bitmapToJpg(commentBitmap!!)) { result ->
            SwoopinAPI.createCommentImage(eventId, newComment, Utility.bitmapToJpg(commentBitmap!!)) { result ->
                // Async results arrived
                endRefreshing()

                // Clear comment fields
                binding.newComment.setText("")
                clearCommentImage()

                if (result != SwoopinAPI.API_SUCCESS)
                {
                    // Log error message
                    val errStr = "Failed to create comment with image: $result"
                    Toast.makeText(this@ViewEventActivity, errStr, Toast.LENGTH_LONG).show()
                    Utility.logDebug(errStr)
                }
                else
                {
                    // Reload event with new comment
                    loadEvent(eventId)
                }
            }
        }
    }

    // ---------------------------------------------------------------------------------------
    //  createComment - Create comment on server
    // ---------------------------------------------------------------------------------------
    private fun createComment()
    {
        // Setup for blocking call
        val newComment = binding.newComment.text.toString()
        startRefreshing()

        // Send comment to server
        runBlocking {
            SwoopinAPI.createComment(eventId, newComment) { result ->
                // Async results arrived
                endRefreshing()

                // Clear comment fields
                binding.newComment.setText("")
                clearCommentImage()

                if (result != SwoopinAPI.API_SUCCESS)
                {
                    // Log error message
                    val errStr = "Failed to create text-only comment: $result"
                    Toast.makeText(this@ViewEventActivity, errStr, Toast.LENGTH_LONG).show()
                    Utility.logDebug(errStr)
                }
                else
                {
                    // Reload event with new comment
                    loadEvent(eventId)
                }
            }
        }
    }
}
