package com.swoopin.android.activity

// Android Stuff
import android.os.Bundle
import android.os.Looper
import android.view.View
import android.app.Activity
import android.widget.Toast
import android.content.Intent
import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.activity.result.contract.ActivityResultContracts

// Google Play Services
import com.google.android.gms.location.*
import com.google.android.gms.maps.GoogleMap

// Swoopin Stuff
import com.swoopin.android.network.Events
import com.swoopin.android.utility.Settings
import com.swoopin.android.network.SwoopinAPI
import com.swoopin.android.utility.Permissions
import com.swoopin.android.adapter.EventListItemAdapter
import com.swoopin.android.application.SwoopinApplication
import com.swoopin.android.databinding.ActivityEventsBinding

// Dark & Light Mode Implementation
// https://proandroiddev.com/implementing-dark-theme-in-your-android-application-ec2b4fefb6e3

/// Recycler View
// https://guides.codepath.com/android/using-the-recyclerview
// https://developer.android.com/guide/topics/ui/layout/recyclerview

// ---------------------------------------------------------------------------------------
//  EventsActivity - Event list activity
// ---------------------------------------------------------------------------------------
class EventsActivity : AppCompatActivity(), EventListItemAdapter.EventItemClickListener, Events.EventCallback
{
    companion object {
        // Track current position of event list
        var currentScrollPosition = 0
    }

    // Activity resources
    private lateinit var binding: ActivityEventsBinding
    private lateinit var eventsAdapter: EventListItemAdapter

    // 0 = SOON, 1 = STARTING, 2 = NEW
    private var sorting: Int = 0

    // Google Play Services
    private lateinit var mainMap: GoogleMap
    private lateinit var locationCallback: LocationCallback
    private lateinit var fusedLocationClient: FusedLocationProviderClient

    // Permissions manager
    private val permissions = Permissions(this)

    // ---------------------------------------------------------------------------------------
    //  onCreate - Initialize activity
    // ---------------------------------------------------------------------------------------
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)

        // Hide top bar (how to do this in XML instead?
        supportActionBar?.hide()

        // Bind to layout
        binding = ActivityEventsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Initialize event list and adapter
        eventsAdapter = EventListItemAdapter(this)
        binding.eventList.adapter = eventsAdapter
        binding.eventList.layoutManager = LinearLayoutManager(this)
        binding.eventList.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))

        // Remember current scroll position
        binding.eventList.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int)
            {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    val layoutManager: LinearLayoutManager = recyclerView.layoutManager as LinearLayoutManager
                    currentScrollPosition = layoutManager.findLastVisibleItemPosition()
                }
            }
        })

        // Initialize (but do not activate) location services
        initLocationServices()
    }

    // ---------------------------------------------------------------------------------------
    //  onPause - Activity is being backgrounded
    // ---------------------------------------------------------------------------------------
    override fun onPause()
    {
        // Pause location services
        Events.unsubscribeRefreshEvents(this)
        pauseLocationServices()
        super.onPause()
    }

    // ---------------------------------------------------------------------------------------
    //  onResume - Activity is being foregrounded
    // ---------------------------------------------------------------------------------------
    override fun onResume()
    {
        // Permissions can disappear at any time, so always check for them when foregrounded
        super.onResume()
        resumeLocationServices()
        Events.subscribeRefreshEvents(this)
    }

    // ---------------------------------------------------------------------------------------
    //  onRequestPermissionsResult - Handle asynchronous results from permission requests
    // ---------------------------------------------------------------------------------------
    override fun onRequestPermissionsResult(requestCode: Int, permList: Array<String>, grantResults: IntArray)
    {
        // Call permissions manager first so flags get set
        permissions.onRequestPermissionsResult(requestCode, permList, grantResults)

        // Call super next so fragments get called to handle their permission checks
        super.onRequestPermissionsResult(requestCode, permList, grantResults)

        // Ask for location permissions until we get it
        resumeLocationServices()
    }

    // ---------------------------------------------------------------------------------------
    //  resumeLocationServices - Subscribe to location updates
    // ---------------------------------------------------------------------------------------
    @SuppressLint("MissingPermission")
    private fun resumeLocationServices()
    {
        // Location permissions required to move forward from this point
        if (!permissions.hasLocationPermission()) {
            // Keep asking until they're given
            permissions.checkLocationPermission()
        }
        else {
            // Request location updates
            fusedLocationClient.requestLocationUpdates(permissions.eventLocationRequest, locationCallback, Looper.getMainLooper())
        }
    }

    // ---------------------------------------------------------------------------------------
    //  pauseLocationServices - Pause location updates
    // ---------------------------------------------------------------------------------------
    private fun pauseLocationServices()
    {
        // Pause location services
        fusedLocationClient.removeLocationUpdates(locationCallback)
    }

    // ---------------------------------------------------------------------------------------
    //  initLocationServices - Initialize location services
    // ---------------------------------------------------------------------------------------
    private fun initLocationServices()
    {
        // Create location services client
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        // Create location services listener
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                locationResult ?: return
                if (locationResult.locations.isNotEmpty())
                {
                    // Update master location
                    SwoopinAPI.setDeviceLocation(locationResult.lastLocation.latitude, locationResult.lastLocation.longitude)

                    // Update event list
                }
            }
        }
    }

    // ---------------------------------------------------------------------------------------
    //  onEventClicked - Get notified when an event item is clicked
    // ---------------------------------------------------------------------------------------
    override fun onEventClicked(eventIndex: Int)
    {
        // Launch "View Event" Activity
        val intent = Intent(this, ViewEventActivity::class.java)
        intent.putExtra(ViewEventActivity.EXTRA_EVENT_ID, eventsAdapter.getEventId(eventIndex))
        startActivity(intent)
    }

    // ---------------------------------------------------------------------------------------
    //  onEventLongPress - Get notified when an event item is long-pressed
    // ---------------------------------------------------------------------------------------
    override fun onEventLongPressed(eventIndex: Int)
    {
        // Launch "Edit Event" Activity

        Toast.makeText(this,"Event Item Long-pressed: ${eventsAdapter.getEventId(eventIndex)}", Toast.LENGTH_SHORT).show()
    }

    // ---------------------------------------------------------------------------------------
    //  onEventsRefreshed - Get notified when the master event list is updated
    // ---------------------------------------------------------------------------------------
    override fun onEventsRefreshed()
    {
        // Notify adapter there's a new set of data
        eventsAdapter.setList(Events.getEventsByStart())

        // Set current scroll position
        binding.eventList.layoutManager?.scrollToPosition(currentScrollPosition)
    }

    // ---------------------------------------------------------------------------------------
    //  closeActivity - Shutdown this activity
    // ---------------------------------------------------------------------------------------
    fun closeActivity()
    {
        // Always reset scroll position before leaving
        currentScrollPosition = 0
        finish()
    }

    // ---------------------------------------------------------------------------------------
    //  modeButtonClick - Toggle between map and event list views
    // ---------------------------------------------------------------------------------------
    @Suppress("UNUSED_PARAMETER")
    fun modeButtonClick(view: View)
    {
        // Set map mode so map will show up
        SwoopinApplication.settings().setBooleanValue(Settings.PREFKEY_MAPMODE, true)
        closeActivity()
    }

    // ---------------------------------------------------------------------------------------
    //  notificationsButtonClick - Go to notifications view
    // ---------------------------------------------------------------------------------------
    @Suppress("UNUSED_PARAMETER")
    fun notificationsButtonClick(view: View)
    {
        Toast.makeText(this,"Notifications not yet implemented", Toast.LENGTH_SHORT).show()

        // Create intent to show notifications view
//        closeActivity()
    }

    // ---------------------------------------------------------------------------------------
    //  createButtonClick - Create new pin
    // ---------------------------------------------------------------------------------------
    @Suppress("UNUSED_PARAMETER")
    fun createButtonClick(view: View)
    {
        Toast.makeText(this,"Create event not yet implemented", Toast.LENGTH_SHORT).show()

        // Create intent to show Create Event view
//        closeActivity()
    }

    // ---------------------------------------------------------------------------------------
    //  groupsButtonClick - Create new pin
    // ---------------------------------------------------------------------------------------
    @Suppress("UNUSED_PARAMETER")
    fun groupsButtonClick(view: View)
    {
        Toast.makeText(this,"Groups not yet implemented", Toast.LENGTH_SHORT).show()

        // Create intent to show Groups view
//        closeActivity()
    }

    // Intent launcher for Settings
    // For some reason this must be a class-level variable
    private val settingsIntentLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_CANCELED) {
                // User logged out
                finish()
            }
        }

    // ---------------------------------------------------------------------------------------
    //  settingsButtonClick - Create new pin
    // ---------------------------------------------------------------------------------------
    @Suppress("UNUSED_PARAMETER")
    fun settingsButtonClick(view: View)
    {
        // New way of launching intent for results
        val settingsIntent = Intent(this, SettingsActivity::class.java)
        settingsIntentLauncher.launch(settingsIntent)
    }
}
