package com.swoopin.android.activity

import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import android.content.Context
import android.content.Intent
import com.swoopin.android.R
import android.os.Bundle

import com.swoopin.android.application.SwoopinApplication
import com.swoopin.android.databinding.ActivityLaunchBinding
import com.swoopin.android.network.SwoopinAPI
import com.swoopin.android.utility.Settings
import kotlinx.coroutines.runBlocking

// ---------------------------------------------------------------------------------------
//  LaunchActivity - State manager for UI flow
// ---------------------------------------------------------------------------------------
class LaunchActivity : AppCompatActivity()
{
    // Static fields
    companion object {
        var forceLogin = false
        var tokenValidated = false
    }

    // Layout bindings
    private lateinit var binding: ActivityLaunchBinding
    private var context: Context = this

    // True when awaiting a Server response
    private var awaitingResponse = false

    // ---------------------------------------------------------------------------------------
    //  onCreate - Activity initialization
    // ---------------------------------------------------------------------------------------
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)

        // Inflate layout
        binding = ActivityLaunchBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    // ---------------------------------------------------------------------------------------
    //  onStart - Logic to run at startup
    // ---------------------------------------------------------------------------------------
    override fun onStart()
    {
        super.onStart()

        // For testing only
//        SwoopinAPI.profileInfo.reset()
//        SwoopinApplication.settings().clear()

        // Determine which activity to launch
        launchNextActivity()
    }

    // ---------------------------------------------------------------------------------------
    //  launchNextActivity - Determine which activity to launch next
    // ---------------------------------------------------------------------------------------
    private fun launchNextActivity()
    {
        // Gather decision data
        val termsAccepted = SwoopinApplication.settings().getBooleanValue(Settings.PREFKEY_TERMS_ACCEPTED, false)
        val email = SwoopinApplication.settings().getStringValue(Settings.PREFKEY_EMAIL, "")
        val token = SwoopinApplication.settings().getStringValue(Settings.PREFKEY_AUTHTOKEN, "")

        if (!termsAccepted)
        {
            // Must accept terms before ANYTHING else (Google Play Store rule)
            val intent = Intent(context, AcceptTermsActivity::class.java)
            context.startActivity(intent)
        }
        else if (email.isEmpty() && !forceLogin)
        {
            // Create an account
            val intent = Intent(context, RegisterActivity::class.java)
            context.startActivity(intent)
        }
        else if (token.isEmpty())
        {
            // Account was created so now log in
            val intent = Intent(context, LoginActivity::class.java)
            context.startActivity(intent)
        }
        else if (!tokenValidated)
        {
            // Display spinner while waiting?
            awaitingResponse = true

            // User has successfully logged in before so just validate token
            runBlocking {
                SwoopinAPI.validateToken() { result ->
                    // Done waiting for response
                    awaitingResponse = false

                    if (result != SwoopinAPI.API_SUCCESS)
                    {
                        // Display error message
                        binding.errorText.text = getString(R.string.msg_invalid_token, result)
                        binding.errorText.isVisible = true
                        tokenValidated = false

                        // Most likely an invalid token so log out and start over
                        SwoopinApplication.instance.logOutUser()
                    }
                    else
                    {
                        // Token received so go to next UI state
                        binding.errorText.isVisible = false
                        binding.errorText.text = ""
                        tokenValidated = true

                        // Call again will skip this section
                        launchNextActivity()
                    }
                }
            }
        }
        else if (!awaitingResponse)
        {
            // Launch in either map or list view
            val mapMode = SwoopinApplication.settings().getBooleanValue(Settings.PREFKEY_MAPMODE, true)
            if (mapMode) {
                val intent = Intent(context, MapsActivity::class.java)
                context.startActivity(intent)
            }
            else {
                val intent = Intent(context, EventsActivity::class.java)
                context.startActivity(intent)
            }
        }
    }
}
