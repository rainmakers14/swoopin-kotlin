@file:Suppress("PrivatePropertyName", "SpellCheckingInspection", "SpellCheckingInspection")

package com.swoopin.android.activity

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.swoopin.android.R
import com.swoopin.android.application.SwoopinApplication
import com.swoopin.android.databinding.ActivityAcceptTermsBinding
import com.swoopin.android.utility.Settings
import com.swoopin.android.utility.Settings.Companion.PREFKEY_TERMS_ACCEPTED
import com.swoopin.android.utility.Utility

class AcceptTermsActivity : AppCompatActivity()
{
    // Log tag for this object
    private val LOGTAG = "AcceptTermsActivity"

    // Layout binding now works differently in Kotlin because they moved from "synthetics" to Jetpack Views
    // https://developer.android.com/topic/libraries/view-binding/migration
    // https://developer.android.com/topic/libraries/view-binding#activities
    private lateinit var binding: ActivityAcceptTermsBinding

    var context: Context? = null
    var settings: Settings = SwoopinApplication.settings()

    // ---------------------------------------------------------------------------------------
    //  onCreate
    // ---------------------------------------------------------------------------------------
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)

        // This is the new way to set content views
        binding = ActivityAcceptTermsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Set local references
        context = this

        // Determine initial state of UI elements
        val termsAccepted = settings.getBooleanValue(PREFKEY_TERMS_ACCEPTED, false)
        binding.acceptButton.isEnabled = termsAccepted
        binding.termsCheckbox.isChecked = termsAccepted
        binding.privacyCheckbox.isChecked = termsAccepted
        binding.disclosureCheckbox.isChecked = termsAccepted

        // Set up checkbox handlers
        binding.termsCheckbox.setOnCheckedChangeListener { _, _ -> checkAcceptance() }
        binding.privacyCheckbox.setOnCheckedChangeListener { _, _ -> checkAcceptance() }
        binding.disclosureCheckbox.setOnCheckedChangeListener { _, _ -> checkAcceptance() }
    }

    // ---------------------------------------------------------------------------------------
    //  onStart
    // ---------------------------------------------------------------------------------------
    override fun onStart() {
        super.onStart()
    }

    // ---------------------------------------------------------------------------------------
    //  onBackPressed
    // ---------------------------------------------------------------------------------------
    override fun onBackPressed() {
        // Disable until terms accepted
        val termsAccepted = settings.getBooleanValue(PREFKEY_TERMS_ACCEPTED, false)
        if (termsAccepted)
            super.onBackPressed()
    }

    // ---------------------------------------------------------------------------------------
    //  privacyButtonClick - Show privacy policy
    // ---------------------------------------------------------------------------------------
    @Suppress("UNUSED_PARAMETER")
    fun privacyButtonClick(view: View) {
        // Display privacy policy
        Utility.openURL(context, getString(R.string.privacy_url))
    }

    // ---------------------------------------------------------------------------------------
    //  termsButtonClick - Show terms & conditions
    // ---------------------------------------------------------------------------------------
    @Suppress("UNUSED_PARAMETER")
    fun termsButtonClick(view: View) {
        // Display terms & conditions
        Utility.openURL(context, getString(R.string.terms_url))
    }

    // ---------------------------------------------------------------------------------------
    //  acceptButtonClick - Handle Accept button being clicked
    // ---------------------------------------------------------------------------------------
    @Suppress("UNUSED_PARAMETER")
    fun acceptButtonClick(view: View) {
        settings.setBooleanValue(PREFKEY_TERMS_ACCEPTED, true)
        finish()
    }

    // ---------------------------------------------------------------------------------------
    //  checkAcceptance - Check if all terms have been accepted
    // ---------------------------------------------------------------------------------------
    private fun checkAcceptance()
    {
        // Everything must be checked before the accept button is active
        val allChecked = binding.disclosureCheckbox.isChecked() && binding.termsCheckbox.isChecked() && binding.privacyCheckbox.isChecked()

        // Update accept button and settings value
        binding.acceptButton.setEnabled(allChecked)
        if (!allChecked)
            settings.setBooleanValue(PREFKEY_TERMS_ACCEPTED, false)
    }
}
