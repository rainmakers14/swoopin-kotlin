package com.swoopin.android.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import com.swoopin.android.R

import com.swoopin.android.application.SwoopinApplication
import com.swoopin.android.databinding.ActivityRegisterBinding
import com.swoopin.android.network.SwoopinAPI
import com.swoopin.android.utility.Settings
import kotlinx.coroutines.runBlocking

// ---------------------------------------------------------------------------------------
//  RegisterActivity - Activity to handle initial user registration
// ---------------------------------------------------------------------------------------
class RegisterActivity : AppCompatActivity()
{
    var context: Context? = null
    var settings: Settings = SwoopinApplication.settings()
    private lateinit var binding: ActivityRegisterBinding
    private var awaitingResponse = false

    // ---------------------------------------------------------------------------------------
    //  onCreate - Initialize everything
    // ---------------------------------------------------------------------------------------
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)

        // Inflate layout
        binding = ActivityRegisterBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Set local references
        context = this

        // Need two fields - first AND last name!

        // Determine initial state of UI elements
        val firstName = settings.getStringValue(Settings.PREFKEY_LASTNAME, "")
        val lastName = settings.getStringValue(Settings.PREFKEY_LASTNAME, "")
        val pwd = settings.getStringValue(Settings.PREFKEY_PASSWORD, "")
        val email = settings.getStringValue(Settings.PREFKEY_EMAIL, "")
        binding.firstName.setText(firstName)
        binding.lastName.setText(lastName)
        binding.emailText.setText(email)
        binding.passwordText.setText(pwd)
        binding.verifyText.setText("")

        // Disable until all fields are filled
        checkRegisterButton()

        // Set text changed handlers to determine when to enable the REGISTER button
        binding.firstName.customAfterTextChanged { _ -> }
        binding.lastName.customAfterTextChanged { _ -> }
        binding.emailText.customAfterTextChanged { _ -> }
        binding.passwordText.customAfterTextChanged { _ -> }
        binding.verifyText.customAfterTextChanged { _ -> }
    }

    // ---------------------------------------------------------------------------------------
    //  customAfterTextChanged - Override text change processor for all text fields
    // ---------------------------------------------------------------------------------------
    @Suppress("UNUSED_PARAMETER")
    private fun EditText.customAfterTextChanged(action: (Editable?)-> Unit) {
        this.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun afterTextChanged(editable: Editable?) { checkRegisterButton() }
        })
    }

    // ---------------------------------------------------------------------------------------
    //  onBackPressed
    // ---------------------------------------------------------------------------------------
    override fun onBackPressed() {
        // Disable because there's no place else to go
    }

    // ---------------------------------------------------------------------------------------
    //  policyButtonClick - Launch policies activity
    // ---------------------------------------------------------------------------------------
    @Suppress("UNUSED_PARAMETER")
    fun policyButtonClick(view: View) {
        val intent = Intent(context, AcceptTermsActivity::class.java)
        startActivity(intent)
    }

    // ---------------------------------------------------------------------------------------
    //  haveAccountButtonClick - Launch login activity
    // ---------------------------------------------------------------------------------------
    @Suppress("UNUSED_PARAMETER")
    fun haveAccountButtonClick(view: View) {
        // Trick the launch activity into going to the login screen instead of this one
        LaunchActivity.forceLogin = true
        finish()
    }

    // ---------------------------------------------------------------------------------------
    //  checkRegisterButton - Check if Register button should be enabled
    // ---------------------------------------------------------------------------------------
    private fun checkRegisterButton()
    {
        // Assume not until proven otherwise
        binding.registerButton.isEnabled = false

        // Set default error text
        val minPwLen: Int = getString(R.string.min_pw_length).toInt()
        if (binding.passwordText.text.length < minPwLen) {
            binding.errorText.text = getString(R.string.msg_pw_length, minPwLen)
            return
        }

        // Passwords must be equal
        if (binding.passwordText.text.toString() != binding.verifyText.text.toString()) {
            binding.errorText.text = getString(R.string.msg_pw_match)
            return
        }

        // Check for empty fields (last name not required)
        if (binding.firstName.text.isEmpty() || binding.emailText.text.isEmpty()) {
            binding.errorText.text = getString(R.string.msg_name_email)
            return
        }

        // Registration data can now be sent
        if (!awaitingResponse) {
            binding.errorText.text = getString(R.string.msg_click_button)
            binding.registerButton.isEnabled = true
        }
    }

    // ---------------------------------------------------------------------------------------
    //  registerButtonClick - Handle Register button being clicked
    // ---------------------------------------------------------------------------------------
    @Suppress("UNUSED_PARAMETER")
    fun registerButtonClick(view: View)
    {
        // Extract values
        val firstName = binding.firstName.text.toString()
        val lastName = binding.lastName.text.toString()
        val email = binding.emailText.text.toString()
        val password = binding.passwordText.text.toString()

        // Save Settings values
        settings.setStringValue(Settings.PREFKEY_FIRSTNAME, firstName)
        settings.setStringValue(Settings.PREFKEY_LASTNAME, lastName)
        settings.setStringValue(Settings.PREFKEY_EMAIL, email)
        settings.setStringValue(Settings.PREFKEY_PASSWORD, password)

        // Prevent repeated calls to create account
        binding.registerButton.isEnabled = false
        awaitingResponse = true

        // Display spinner while waiting?

        // Validate credentials
        runBlocking {
            SwoopinAPI.createProfile(firstName, lastName, email, password) { result ->
                // Re-enable Register button
                awaitingResponse = false

                if (result != SwoopinAPI.API_SUCCESS) {
                    // Display error message
                    binding.errorText.text = result

                    // Reset user data
                    settings.setStringValue(Settings.PREFKEY_FIRSTNAME, "")
                    settings.setStringValue(Settings.PREFKEY_LASTNAME, "")
                    settings.setStringValue(Settings.PREFKEY_EMAIL, "")
                    settings.setStringValue(Settings.PREFKEY_PASSWORD, "")
                }
                else {
                    // Account created, go to next UI state
                    binding.errorText.text = ""
                    finish()
                }
            }
        }
    }
}
