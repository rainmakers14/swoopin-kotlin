package com.swoopin.android.activity

// Android Stuff
import android.os.Bundle
import android.os.Looper
import android.view.View
import android.app.Activity
import android.widget.Toast
import kotlinx.coroutines.*
import android.content.Intent
import android.content.Context
import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.activity.result.contract.ActivityResultContracts

// Google Play Services
import com.google.android.gms.location.*
import com.google.android.gms.maps.model.*
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.clustering.Cluster
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.maps.android.clustering.ClusterItem
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.MarkerOptions
import com.google.maps.android.clustering.ClusterManager
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.maps.android.clustering.view.DefaultClusterRenderer

// Swoopin Stuff
import com.swoopin.android.R
import com.swoopin.android.network.*
import com.swoopin.android.utility.Settings
import com.swoopin.android.utility.Permissions
import com.swoopin.android.adapter.PinMapAdapter
import com.swoopin.android.adapter.EventListItemAdapter
import com.swoopin.android.network.Events.EventCallback
import com.swoopin.android.application.SwoopinApplication
import com.swoopin.android.databinding.ActivityMainMapBinding
import com.swoopin.android.activity.ViewEventActivity.Companion.EXTRA_EVENT_ID

// Dark & Light Mode Implementation
// https://proandroiddev.com/implementing-dark-theme-in-your-android-application-ec2b4fefb6e3

// Emoji library
// https://medium.com/exploring-android/exploring-the-android-emoji-compatibility-library-1b9f3bb724aa

// Google map utility libraries
// https://developers.google.com/maps/documentation/android-sdk/utility

// ---------------------------------------------------------------------------------------
//  MapsActivity - Main map activity
// ---------------------------------------------------------------------------------------
class MapsActivity : AppCompatActivity(),
    EventCallback,
    OnMapReadyCallback,
    GoogleMap.OnMapClickListener,
    GoogleMap.OnMapLongClickListener,
    EventListItemAdapter.EventItemClickListener,
    ClusterManager.OnClusterClickListener<MapsActivity.EventClusterItem>,
    ClusterManager.OnClusterItemClickListener<MapsActivity.EventClusterItem>,
    ClusterManager.OnClusterItemInfoWindowClickListener<MapsActivity.EventClusterItem>
{
    // Custom cluster item
    inner class EventClusterItem(private val position: LatLng, private val title: String,
                                 private val description: String, evId: Int): ClusterItem
    {
        val eventId = evId
        override fun getPosition(): LatLng { return position }
        override fun getTitle(): String { return title }
        override fun getSnippet(): String { return description }
    }

    // Custom cluster item renderer
    inner class EventClusterRenderer(context: Context, map: GoogleMap, manager: ClusterManager<MapsActivity.EventClusterItem>):
        DefaultClusterRenderer<EventClusterItem>(context, map, manager)
    {
        // Render custom Marker icon
        override fun onBeforeClusterItemRendered(clusterItem: EventClusterItem, markerOptions: MarkerOptions) {
            // Get custom icon
            val bmap = Events.getEventIcon(clusterItem.eventId)
            if (bmap != null)
                markerOptions.icon(BitmapDescriptorFactory.fromBitmap(bmap))
        }

        // Set marker tag so info-window adapter knows what to do
        override fun onClusterItemRendered(clusterItem: EventClusterItem, marker: Marker) {
            marker.tag = clusterItem.eventId
            super.onClusterItemRendered(clusterItem, marker)
        }
    }

    // Activity resources
    private lateinit var binding: ActivityMainMapBinding

    // Track current position of event list
    var currentEventsPosition = 0

    // Google Play Services
    private lateinit var mainMap: GoogleMap
    private lateinit var locationCallback: LocationCallback
    private lateinit var fusedLocationClient: FusedLocationProviderClient

    // Map info window adapters
    private lateinit var clusterManager: ClusterManager<EventClusterItem>
    private lateinit var eventsAdapter: EventListItemAdapter
    private lateinit var pinMapAdapter: PinMapAdapter

    // Map styles
    var activeMapStyle = 0
    val mapStyle = intArrayOf(R.raw.maps_style_aubergine, R.raw.maps_style_dark, R.raw.maps_style_night,
        R.raw.maps_style_retro, R.raw.maps_style_silver, R.raw.maps_style_standard)
    val mapStyleName = arrayOf("Aubergine", "Dark", "Night", "Retro", "Silver", "Standard")

    // Map parameters
    var defaultMapZoom = 12.0f
    var defaultMapTilt = 17.0f
    var locationDisplayed = false

    // Permissions manager
//    val context: Context = this
    private val permissions = Permissions(this)

    // ---------------------------------------------------------------------------------------
    //  onCreate - Initialize activity
    // ---------------------------------------------------------------------------------------
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)

        // Hide top bar (how to do this in XML instead?
        supportActionBar?.hide()

        // Bind layout resources
        binding = ActivityMainMapBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Obtain the SupportMapFragment and get notified when the map is ready to be used
        val mapFragment = supportFragmentManager.findFragmentById(R.id.mainMap) as SupportMapFragment
        mapFragment.getMapAsync(this)

        // Map pin info window adapter
        pinMapAdapter = PinMapAdapter(this)

        // Initialize event list and adapter
        eventsAdapter = EventListItemAdapter(this)
        binding.eventList.adapter = eventsAdapter
        binding.eventList.layoutManager = LinearLayoutManager(this)
        binding.eventList.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))

        // Remember current scroll position
        binding.eventList.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    val layoutManager: LinearLayoutManager = recyclerView.layoutManager as LinearLayoutManager
                    currentEventsPosition = layoutManager.findLastVisibleItemPosition()
                }
            }
        })

        // Initialize (but do not activate) location services
        initLocationServices()
    }

    // ---------------------------------------------------------------------------------------
    //  onPause - Activity is being backgrounded
    // ---------------------------------------------------------------------------------------
    override fun onPause()
    {
        // Pause location services
        Events.unsubscribeRefreshEvents(this)
        pauseLocationServices()
        super.onPause()
    }

    // ---------------------------------------------------------------------------------------
    //  onResume - Activity is being foregrounded
    // ---------------------------------------------------------------------------------------
    override fun onResume()
    {
        // Permissions can disappear at any time, so always check for them when foregrounded
        super.onResume()
        resumeLocationServices()
        Events.subscribeRefreshEvents(this)
    }

    // ---------------------------------------------------------------------------------------
    //  onBackPressed
    // ---------------------------------------------------------------------------------------
    override fun onBackPressed()
    {
        // Close cluster popup if it's active
        if (eventsAdapter.itemCount > 0)
            eventsAdapter.clear()
        else
            super.onBackPressed()
    }

    // ---------------------------------------------------------------------------------------
    //  onRequestPermissionsResult - Handle asynchronous results from permission requests
    // ---------------------------------------------------------------------------------------
    override fun onRequestPermissionsResult(requestCode: Int, permList: Array<String>, grantResults: IntArray)
    {
        // Call permissions manager first so flags get set
        permissions.onRequestPermissionsResult(requestCode, permList, grantResults)

        // Call super next so fragments get called to handle their permission checks
        super.onRequestPermissionsResult(requestCode, permList, grantResults)

        // Ask for location permissions until we get it
        resumeLocationServices()
    }

    // ---------------------------------------------------------------------------------------
    //  resumeLocationServices - Subscribe to location updates
    // ---------------------------------------------------------------------------------------
    @SuppressLint("MissingPermission")
    private fun resumeLocationServices()
    {
        // Location permissions required to move forward from this point
        if (!permissions.hasLocationPermission()) {
            // Keep asking until they're given
            permissions.checkLocationPermission()
        }
        else {
            // Request location updates
            fusedLocationClient.requestLocationUpdates(permissions.eventLocationRequest, locationCallback, Looper.getMainLooper())
        }
    }

    // ---------------------------------------------------------------------------------------
    //  pauseLocationServices - Pause location updates
    // ---------------------------------------------------------------------------------------
    private fun pauseLocationServices()
    {
        // Pause location services
        fusedLocationClient.removeLocationUpdates(locationCallback)
    }

    // ---------------------------------------------------------------------------------------
    //  initLocationServices - Initialize location services
    // ---------------------------------------------------------------------------------------
    private fun initLocationServices()
    {
        // Create location services client
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        // Create location services listener
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                locationResult ?: return
                if (locationResult.locations.isNotEmpty())
                {
                    // Update master location
                    SwoopinAPI.setDeviceLocation(locationResult.lastLocation.latitude, locationResult.lastLocation.longitude)

                    // Move camera to current device location, but only once at beginning of session
                    if (!locationDisplayed) {
                        mainMap.moveCamera(CameraUpdateFactory.newLatLngZoom(SwoopinAPI.deviceLocation!!, defaultMapZoom))
                        locationDisplayed = true
                    }

                    // Enable ability to show device location on the map
                    // (permissions may not have been available when onMapReady() was called)
                    enableMyLocation()
                }
            }
        }
    }

    // ---------------------------------------------------------------------------------------
    //  enableMyLocation - Enable "my location" once both permissions and map are ready
    // ---------------------------------------------------------------------------------------
    @SuppressLint("MissingPermission")
    private fun enableMyLocation()
    {
        // Enable ability to show device location on the map
        if (::mainMap.isInitialized && permissions.hasLocationPermission() && !mainMap.isMyLocationEnabled)
            mainMap.isMyLocationEnabled = true
    }

    // ---------------------------------------------------------------------------------------
    //  onMapReady - Configure map view once it's available
    // ---------------------------------------------------------------------------------------
    override fun onMapReady(googleMap: GoogleMap)
    {
        // Set map reference
        mainMap = googleMap

        // Set map click listeners
        mainMap.setOnMapClickListener(this)
        mainMap.setOnMapLongClickListener(this)

        // Set custom map style
        mainMap.mapType = GoogleMap.MAP_TYPE_NORMAL
        mainMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, mapStyle[activeMapStyle]))

        // Enable ability to show device location on the map
        enableMyLocation()

        // Disable Google's compass button that scrolls map to current location
        mainMap.uiSettings.isMyLocationButtonEnabled = false
        mainMap.uiSettings.isCompassEnabled = false

        // Disable other map controls (these don't seem to do anything???)
        mainMap.uiSettings.isZoomControlsEnabled = false
        mainMap.uiSettings.isMapToolbarEnabled = false

        // Move camera to current device location, but only once at beginning of session
        if (SwoopinAPI.deviceLocation != null) {
            mainMap.moveCamera(CameraUpdateFactory.newLatLngZoom(SwoopinAPI.deviceLocation!!, defaultMapZoom))
            locationDisplayed = true
        }

        // Initialize cluster manager
        clusterManager = ClusterManager(this, mainMap)
        clusterManager.renderer = EventClusterRenderer(this, mainMap, clusterManager)
        clusterManager.setOnClusterItemClickListener(this)
        clusterManager.setOnClusterClickListener(this)

        // Set camera listener
        mainMap.setOnCameraIdleListener(clusterManager)

        // Set custom map infowindow adapters
        mainMap.setInfoWindowAdapter(clusterManager.markerManager)
        clusterManager.setOnClusterItemInfoWindowClickListener(this)
        clusterManager.markerCollection.setInfoWindowAdapter(pinMapAdapter)

        // Add overlay for local event
        addOverlay()
    }

    // ---------------------------------------------------------------------------------------
    //  addOverlay - Add overlay for local event (total kludge for now)
    // ---------------------------------------------------------------------------------------
    private fun addOverlay()
    {
        // Set overlay bounds
        val southWest = LatLng(34.067062, -117.650716)
        val northEast = LatLng(34.067333, -117.650252)
        val bounds: LatLngBounds = LatLngBounds(southWest, northEast)

        // Set overlay options
        val eventOverlay = GroundOverlayOptions()
            .image(BitmapDescriptorFactory.fromResource(R.mipmap.event_overlay))
            .positionFromBounds(bounds)
            .transparency(0.6F)
            .clickable(false)
            .bearing(0F)

        mainMap.addGroundOverlay(eventOverlay)
    }

    // ---------------------------------------------------------------------------------------
    //  onEventsRefreshed - Get notified when the master event list is updated
    // ---------------------------------------------------------------------------------------
    override fun onEventsRefreshed()
    {
        // Events have been refreshed so clear all pins & reload
        clusterManager.clearItems()
        clusterManager.cluster()
        eventsAdapter.clear()

        // Create new pins for the new events
        Events.eventMap.forEach { (_, value) ->
            val event: EventInfo = value
            val clusterItem = EventClusterItem(event.getLatLng(), event.eventActivity, event.eventDesc, event.id)
            clusterManager.addItem(clusterItem)
        }

        // Show new cluster items
        clusterManager.cluster()
    }

    // ---------------------------------------------------------------------------------------
    //  modeButtonClick - Toggle between map and event list views
    // ---------------------------------------------------------------------------------------
    @Suppress("UNUSED_PARAMETER")
    fun modeButtonClick(view: View)
    {
        // Set map mode so event list will show up
        SwoopinApplication.settings().setBooleanValue(Settings.PREFKEY_MAPMODE, false)
        finish()
    }

    // ---------------------------------------------------------------------------------------
    //  notificationsButtonClick - Go to notifications view
    // ---------------------------------------------------------------------------------------
    @Suppress("UNUSED_PARAMETER")
    fun notificationsButtonClick(view: View)
    {
        Toast.makeText(this,"Notifications not yet implemented", Toast.LENGTH_SHORT).show()

        // Create intent to show notifications view
//        finish()
    }

    // ---------------------------------------------------------------------------------------
    //  createButtonClick - Create new pin
    // ---------------------------------------------------------------------------------------
    @Suppress("UNUSED_PARAMETER")
    fun createButtonClick(view: View)
    {
        Toast.makeText(this,"Create event not yet implemented", Toast.LENGTH_SHORT).show()

        // Create intent to show Create Event view
//        finish()
    }

    // ---------------------------------------------------------------------------------------
    //  groupsButtonClick - Create new pin
    // ---------------------------------------------------------------------------------------
    @Suppress("UNUSED_PARAMETER")
    fun groupsButtonClick(view: View)
    {
        Toast.makeText(this,"Groups not yet implemented", Toast.LENGTH_SHORT).show()

        // Create intent to show Groups view
//        finish()
    }

    // Intent launcher for Settings
    // For some reason this must be a class-level variable
    private val settingsIntentLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_CANCELED) {
                // User logged out
                finish()
            }
        }

    // ---------------------------------------------------------------------------------------
    //  settingsButtonClick - Create new pin
    // ---------------------------------------------------------------------------------------
    @Suppress("UNUSED_PARAMETER")
    fun settingsButtonClick(view: View)
    {
        // New way of launching intent for results
        val settingsIntent = Intent(this, SettingsActivity::class.java)
        settingsIntentLauncher.launch(settingsIntent)
    }

    // ---------------------------------------------------------------------------------------
    //  onClusterClick - Handle click on a cluster
    // ---------------------------------------------------------------------------------------
    override fun onClusterClick(cluster: Cluster<EventClusterItem>): Boolean
    {
        // Reset scroll position
        currentEventsPosition = 0
        eventsAdapter.clear()

        // Add events to list
        val eventList = mutableListOf<EventInfo>()
        for (item in cluster.items) {
            val event = Events.eventMap[item.eventId]
            if (event != null)
                eventList.add(event)
        }

        // Show the specified event list
        eventsAdapter.setList(eventList)
        return false
    }

    // ---------------------------------------------------------------------------------------
    //  onClusterItemClick - Handle click on a cluster item
    // ---------------------------------------------------------------------------------------
    override fun onClusterItemClick(clusterItem: EventClusterItem): Boolean
    {
        // Let parent handle most of the behavior
        return false
    }

    // ---------------------------------------------------------------------------------------
    //  onEventClicked - Get notified when an event item is clicked
    // ---------------------------------------------------------------------------------------
    override fun onEventClicked(eventIndex: Int)
    {
        // Launch "View Event" Activity
        val intent = Intent(this, ViewEventActivity::class.java)
        intent.putExtra(EXTRA_EVENT_ID, eventsAdapter.getEventId(eventIndex))
        startActivity(intent)
    }

    // ---------------------------------------------------------------------------------------
    //  onEventLongPress - Get notified when an event item is long-pressed
    // ---------------------------------------------------------------------------------------
    override fun onEventLongPressed(eventIndex: Int)
    {
        // Launch "Edit Event" Activity

        Toast.makeText(this,"Event Item Long-pressed: ${eventsAdapter.getEventId(eventIndex)}", Toast.LENGTH_SHORT).show()
    }

    // ---------------------------------------------------------------------------------------
    //  onClusterItemInfoWindowClick - Handle click on a Marker info window
    // ---------------------------------------------------------------------------------------
    override fun onClusterItemInfoWindowClick(clusterItem: EventClusterItem)
    {
        // InfoWindow click shows event
        val intent = Intent(this, ViewEventActivity::class.java)
        intent.putExtra(EXTRA_EVENT_ID, clusterItem.eventId)
        startActivity(intent)
    }

    // ---------------------------------------------------------------------------------------
    //  onMapClick - Handle click on map
    // ---------------------------------------------------------------------------------------
    override fun onMapClick(point: LatLng)
    {
        // Close cluster popup if it's active
        if (eventsAdapter.itemCount > 0) {
            eventsAdapter.clear()
            return
        }

        // Add black marker for some period of time then remove it
        val helpMsg = getString(R.string.help_hold_to_create)
        val blackPin = BitmapDescriptorFactory.fromResource(R.mipmap.ic_pin_black)
        val markerOptions = MarkerOptions().position(LatLng(point.latitude, point.longitude)).title(helpMsg).icon(blackPin)
        val marker = mainMap.addMarker(markerOptions)

        // Leave help message on screen for a short time
        Toast.makeText(this@MapsActivity, helpMsg, Toast.LENGTH_SHORT).show()
        val twoSeconds: Long = 2500
        GlobalScope.launch {
            delay(twoSeconds)
            this@MapsActivity.runOnUiThread {
                marker?.remove()
            }
        }
    }

    // ---------------------------------------------------------------------------------------
    //  onMapLongClick - Handle long click on map
    // ---------------------------------------------------------------------------------------
    override fun onMapLongClick(point: LatLng)
    {
        // Go to create event activity, pass LatLng

//        mainMap.addMarker(MarkerOptions().position(point).title("You are here")
//                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)))

    }
}
