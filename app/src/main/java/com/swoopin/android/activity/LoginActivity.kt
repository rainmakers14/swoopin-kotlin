package com.swoopin.android.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible

import com.swoopin.android.application.SwoopinApplication
import com.swoopin.android.databinding.ActivityLoginBinding
import com.swoopin.android.network.SwoopinAPI
import com.swoopin.android.utility.Settings
import kotlinx.coroutines.runBlocking

// ---------------------------------------------------------------------------------------
//  LoginActivity - Activity to handle user login
// ---------------------------------------------------------------------------------------
class LoginActivity : AppCompatActivity()
{
    var context: Context? = null
    private var awaitingResponse = false
    val settings = SwoopinApplication.settings()
    private lateinit var binding: ActivityLoginBinding

    // ---------------------------------------------------------------------------------------
    //  onCreate - Initialize everything
    // ---------------------------------------------------------------------------------------
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)

        // Inflate layout
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Set local references
        context = this

        // Determine initial state of UI elements
        val email = settings.getStringValue(Settings.PREFKEY_EMAIL, "")
        binding.emailText.setText(email)

        // Disable until all fields are filled
        checkLoginButton()

        // Set text changed handlers to determine when to enable the REGISTER button
        binding.emailText.customAfterTextChanged { _ -> }
        binding.passwordText.customAfterTextChanged { _ -> }

        // Reset the "Already have an account" flag
        LaunchActivity.forceLogin = false
    }

    // ---------------------------------------------------------------------------------------
    //  customAfterTextChanged - Override text change processor for all text fields
    // ---------------------------------------------------------------------------------------
    @Suppress("UNUSED_PARAMETER")
    private fun EditText.customAfterTextChanged(action: (Editable?)-> Unit) {
        this.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun afterTextChanged(editable: Editable?) { checkLoginButton() }
        })
    }

    // ---------------------------------------------------------------------------------------
    //  onBackPressed
    // ---------------------------------------------------------------------------------------
//    override fun onBackPressed() {
//        // Disable because there's no place else to go
//    }

    // ---------------------------------------------------------------------------------------
    //  checkLoginButton - Check if Login button should be enabled
    // ---------------------------------------------------------------------------------------
    private fun checkLoginButton()
    {
        // Check if all text fields are full
        binding.loginButton.isEnabled = true
        if (binding.emailText.text.isEmpty() || binding.passwordText.text.isEmpty() || awaitingResponse)
            binding.loginButton.isEnabled = false
    }

    // ---------------------------------------------------------------------------------------
    //  forgotButtonClick - Launch forgot password activity
    // ---------------------------------------------------------------------------------------
    @Suppress("UNUSED_PARAMETER")
    fun forgotButtonClick(view: View) {
        // Display message saying password reset message has been sent to your email address?
    }

    // ---------------------------------------------------------------------------------------
    //  policyButtonClick - Launch policies activity
    // ---------------------------------------------------------------------------------------
    @Suppress("UNUSED_PARAMETER")
    fun policyButtonClick(view: View) {
        val intent = Intent(context, AcceptTermsActivity::class.java)
        startActivity(intent)
    }

    // ---------------------------------------------------------------------------------------
    //  loginButtonClick - Handle Login button
    // ---------------------------------------------------------------------------------------
    @Suppress("UNUSED_PARAMETER")
    fun loginButtonClick(view: View)
    {
        // Extract values
        val email = binding.emailText.text.toString()
        val password = binding.passwordText.text.toString()
        settings.setStringValue(Settings.PREFKEY_EMAIL, email)
        settings.setStringValue(Settings.PREFKEY_PASSWORD, password)

        // Prevent repeated login calls
        binding.loginButton.isEnabled = false
        awaitingResponse = true

        // Display spinner while waiting?

        // Validate login credentials
        runBlocking {
            SwoopinAPI.logIn(email, password) { result ->
                // Re-enable Login button
                awaitingResponse = false

                if (result != SwoopinAPI.API_SUCCESS)
                {
                    // Display error message
                    binding.errorText.text = result
                    binding.errorText.isVisible = true
                }
                else
                {
                    // Token received so go to next UI state
                    binding.errorText.isVisible = false
                    binding.errorText.text = ""
                    finish()
                }
            }
        }
    }
}
