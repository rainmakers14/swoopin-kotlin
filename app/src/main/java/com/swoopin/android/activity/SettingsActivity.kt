package com.swoopin.android.activity

// Android stuff
import android.os.Bundle
import android.view.View
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.tabs.TabLayoutMediator

// Swoopin stuff
import com.swoopin.android.utility.Settings
import com.swoopin.android.network.SwoopinAPI
import com.swoopin.android.adapter.SettingsAdapter
import com.swoopin.android.application.SwoopinApplication
import com.swoopin.android.fragment.ZoomOutPageTransformer
import com.swoopin.android.databinding.ActivitySettingsBinding

// ViewPager with multiple layouts
//      https://stackoverflow.com/questions/18413309/how-to-implement-a-viewpager-with-different-fragments-layouts

// ---------------------------------------------------------------------------------------
//  SettingsActivity - Activity to handle settings, ToS, tutorial, etc.
// ---------------------------------------------------------------------------------------
class SettingsActivity : AppCompatActivity()
{
    var context: Context? = null
    var settings: Settings = SwoopinApplication.settings()
    private lateinit var binding: ActivitySettingsBinding

    // ---------------------------------------------------------------------------------------
    //  onCreate - Initialize everything
    // ---------------------------------------------------------------------------------------
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)

        // Inflate layout
        binding = ActivitySettingsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Turn off the title bar
        supportActionBar?.hide()

        // Set local references
        context = this

        // Create settings adapter
        val settingsAdapter = SettingsAdapter(this)
        binding.viewPager.adapter = settingsAdapter
        binding.viewPager.setPageTransformer(ZoomOutPageTransformer())

        // Attach tab mediator (page selectors)
        val tabLayoutMediator = TabLayoutMediator(binding.tabLayout, binding.viewPager, true) { tab, position -> }
        tabLayoutMediator.attach()
    }

    // ---------------------------------------------------------------------------------------
    //  onBackPressed - Handle Back button presses
    // ---------------------------------------------------------------------------------------
    override fun onBackPressed()
    {
        // Go backwards until hitting the first page then exit Activity
        if (binding.viewPager.currentItem > 0)
            binding.viewPager.currentItem = binding.viewPager.currentItem - 1
        else
            super.onBackPressed()
    }

    // ---------------------------------------------------------------------------------------
    //  logoutButtonClick - Handle Logout button
    // ---------------------------------------------------------------------------------------
    @Suppress("UNUSED_PARAMETER")
    fun logoutButtonClick(view: View)
    {
        // Log out the user
        SwoopinApplication.instance.logOutUser()

        // Result causes cascade of finishes, back to LaunchActivity
        setResult(RESULT_CANCELED)
        finish()
    }
}
