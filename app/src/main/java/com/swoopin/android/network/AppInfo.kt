package com.swoopin.android.network

import com.swoopin.android.application.SwoopinApplication
import com.swoopin.android.utility.Settings

import java.util.*
import android.os.Build
import org.json.JSONObject
import org.json.JSONException

import kotlinx.serialization.Serializable

// ---------------------------------------------------------------------------------------
//  AppInfo - Application info. and transformations on that data
// ---------------------------------------------------------------------------------------
@Serializable
class AppInfo
{
    var adid = "00000000-0000-0000-0000-000000000000"
    var systemVersion = Build.VERSION.BASE_OS
    var appVersion = Build.VERSION.RELEASE
    var localizedModel = Build.MANUFACTURER
    var model = Build.MANUFACTURER
    var name = Build.MODEL
    var platformType = "Device"
    var systemName = "Android"
    var uuid = ""

    // ---------------------------------------------------------------------------------------
    //  init - Set all values based on environment
    // ---------------------------------------------------------------------------------------
    fun init()
    {
        // Check magic values from the Internet to see if running in emulator (not guaranteed to work)
        if (Build.HARDWARE.contains("goldfish") || Build.HARDWARE.contains("ranchu"))
            platformType = "Emulator"

        // Set or create UUID
        val settings = SwoopinApplication.settings()
        uuid = settings.getStringValue(Settings.PREFKEY_UUID, uuid)
        if (uuid.isEmpty()) {
            uuid = UUID.randomUUID().toString()
            settings.setStringValue(Settings.PREFKEY_UUID, uuid)
        }
    }

    // ---------------------------------------------------------------------------------------
    //  JSON - Get object as JSON
    // ---------------------------------------------------------------------------------------
    val jSON: JSONObject
        get() {
            val obj = JSONObject()
            try {
                obj.put("adid", adid)
                obj.put("appVersion", appVersion)
                obj.put("localizedModel", localizedModel)
                obj.put("model", model)
                obj.put("name", name)
                obj.put("platformType", platformType)
                obj.put("systemName", systemName)
                obj.put("systemVersion", systemVersion)
                obj.put("uuid", uuid)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            return obj
        }
}
