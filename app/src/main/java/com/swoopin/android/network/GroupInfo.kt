package com.swoopin.android.network

import com.swoopin.android.utility.Utility
import org.json.JSONArray
import org.json.JSONObject
import org.json.JSONException

// ---------------------------------------------------------------------------------------
//  GroupInfo - Event info. from an Event
// ---------------------------------------------------------------------------------------
class GroupInfo
{
    var id = 0
    var groupName = ""
    var groupType = ""
    var groupDesc = ""
    var addDttm = ""
    var lastupdateDttm = ""
    var userStatus = ""
    var approvedSubmitters = 0
    var fileId = 0
    var file: FileInfo? = null
    var members = hashMapOf<Int, UserInfo>()

    init
    {
        // Set default timestamp
        setTimeStamp()
    }

    // ---------------------------------------------------------------------------------------
    //  setTimeStamp - Set time stamp of object
    // ---------------------------------------------------------------------------------------
    fun setTimeStamp() {
        lastupdateDttm = addDttm
        addDttm = Utility.getTimeStamp(null)
    }

    // ---------------------------------------------------------------------------------------
    //  getFileName - Get name of specified file
    // ---------------------------------------------------------------------------------------
    fun getFileName(fileId: Int): String {
        var rc = ""
        if ((file != null) && (file!!.id == fileId))
            rc = file!!.fileName
        return rc
    }

    // ---------------------------------------------------------------------------------------
    //  JSON - Get object as JSON
    // ---------------------------------------------------------------------------------------
    val jSON: JSONObject
        get()
        {
            setTimeStamp()
            val groupObj = JSONObject()
            try
            {
                // Parse normal fields
                groupObj.put("id", id)
                groupObj.put("groupName", groupName)
                groupObj.put("groupType", groupType)
                groupObj.put("groupDesc", groupDesc)
                groupObj.put("addDttm", addDttm)
                groupObj.put("lastupdateDttm", lastupdateDttm)
                groupObj.put("userStatus", userStatus)
                groupObj.put("approvedSubmitters", approvedSubmitters)
                groupObj.put("fileId", fileId)

                // Parse optional 'file' field
                if (file != null)
                    groupObj.put("file", file)

                // Parse members
                val userArray = JSONArray()
                members.forEach { (_, value) ->
                    val userObj: UserInfo = value
                    userArray.put(userObj)
                }
                groupObj.put("members", userArray)
            }
            catch (e: JSONException)
            {
                e.printStackTrace()
            }

            return groupObj
        }

    // ---------------------------------------------------------------------------------------
    //  setValuesFromResponse - Set field values from an API response object
    // ---------------------------------------------------------------------------------------
    fun setValuesFromResponse(groupObj: JSONObject): GroupInfo
    {
        try
        {
            // Parse normal fields
            id = groupObj.optInt("id", id)
            groupName = groupObj.optString("groupName", groupName)
            groupType = groupObj.optString("groupType", groupType)
            groupDesc = groupObj.optString("groupDesc", groupDesc)
            addDttm = groupObj.optString("addDttm", addDttm)
            lastupdateDttm = groupObj.optString("lastupdateDttm", lastupdateDttm)
            userStatus = groupObj.optString("userStatus", userStatus)
            approvedSubmitters = groupObj.optInt("approvedSubmitters", approvedSubmitters)
            fileId = groupObj.optInt("fileId", fileId)

            // Parse optional 'file' field
            if (groupObj.has("file")) {
                val fileObj = groupObj.getJSONObject("file")
                file = FileInfo().setValuesFromResponse(fileObj)
            }

            // Parse group members
            val userArray: JSONArray = groupObj.getJSONArray("members")
            members.clear()
            for (idx in 0 until userArray.length())
            {
                // Parse user
                val userObj = userArray.getJSONObject(idx)
                val userInfo = UserInfo().setValuesFromResponse(userObj)
                members[userInfo.id] = userInfo
            }
        }
        catch (ex: JSONException)
        {
            ex.printStackTrace()
        }

        return this
    }
}
