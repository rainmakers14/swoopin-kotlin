package com.swoopin.android.network

// Java stuff
import java.util.*
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

// JSON stuff
import org.json.JSONException
import org.json.JSONObject
import org.json.JSONArray

// Android stuff
import kotlinx.coroutines.runBlocking
import android.graphics.BitmapFactory
import kotlin.collections.HashMap
import android.graphics.Bitmap

// Ktor stuff
import io.ktor.http.*
import io.ktor.client.call.*
import io.ktor.client.request.*
import io.ktor.client.statement.*

// Swoopin stuff
import com.swoopin.android.application.SwoopinApplication
import com.swoopin.android.utility.Settings
import com.swoopin.android.utility.Utility
import java.time.format.FormatStyle
import com.swoopin.android.R

// ---------------------------------------------------------------------------------------
//  Users - Singleton instance of all Swoopin API User data
// ---------------------------------------------------------------------------------------
object Users
{
    // Refresh user listeners
    interface UserCallback {
        fun onUsersRefreshed()
    }

    // True while refreshing event list
    private var refreshing = false

    // Current list of users
    val userMap = hashMapOf<Int, UserInfo>()

    // List of subscribers to refresh events
    private val refreshListeners: MutableSet<UserCallback> = mutableSetOf()

    // ---------------------------------------------------------------------------------------
    //  subscribeRefreshUsers - Subscribe to know when users have been updated
    // ---------------------------------------------------------------------------------------
    fun subscribeRefreshUsers(listener: UserCallback) {
        refreshListeners += listener
    }

    // ---------------------------------------------------------------------------------------
    //  unsubscribeRefreshUsers - Subscribe to know when events have been updated
    // ---------------------------------------------------------------------------------------
    fun unsubscribeRefreshUsers(listener: UserCallback) {
        refreshListeners -= listener
    }

    // ---------------------------------------------------------------------------------------
    //  onUserRefreshed - Notify all subscribers
    // ---------------------------------------------------------------------------------------
    fun onUsersRefreshed() {
        // Notify every subscriber
        for (callback in refreshListeners) {
            callback.onUsersRefreshed()
        }
    }

    // ---------------------------------------------------------------------------------------
    //  getName - Get name of the specified user
    // ---------------------------------------------------------------------------------------
    fun getName(userId: Int): String
    {
        // Check if we have it locally
        var rc = ""
        val user = userMap[userId]
        if (user != null) {
            rc = user.firstName
        }
        else
        {
            // Request from server
            refreshing = true
            runBlocking {
                getUser(userId) { result ->
                    // Async results arrived
                    refreshing = false
                    if (result != SwoopinAPI.API_SUCCESS) {
                        // Log error message
                        val errStr = "Failed to get user $userId: $result"
                        Utility.logDebug(errStr)
                        refreshing = false
                        onUsersRefreshed()
                    }
                }
            }
        }

        return rc
    }

    // ---------------------------------------------------------------------------------------
    //  getIcon - Get icon of the specified user
    // ---------------------------------------------------------------------------------------
    fun getIcon(userId: Int): String
    {
        // Check if we have it locally
        var rc = ""
        val user = userMap[userId]
        if ((user != null) && (user.file != null))
            rc = user.file!!.fileName
        return rc
    }

    // ---------------------------------------------------------------------------------------
    //  getUser - Get the specified user
    // ---------------------------------------------------------------------------------------
    suspend fun getUser(userId: Int, resultCallback: (result: String) -> Unit)
    {
        // Update profile info. values
        SwoopinAPI.profileInfo.setTimeStamp()

        // Create request JSON
        val jsonBody = JSONObject()
        val jsonRequest = JSONObject()
        jsonRequest.put("appinfo", SwoopinAPI.appInfo.jSON)
        jsonRequest.put("profile", SwoopinAPI.profileInfo.jSON)
        jsonRequest.put("userId", userId)
        jsonBody.put("request", jsonRequest)

        refreshing = true
        val response: HttpResponse = SwoopinAPI.httpClient.post(SwoopinAPI.URLGetUser) {
            headers {
                append(HttpHeaders.ContentType, "application/json")
                append(HttpHeaders.CacheControl, "no-cache")
                append("x-swoopin-authtoken", SwoopinApplication.settings().getStringValue(Settings.PREFKEY_AUTHTOKEN, ""))
                append("x-swoopin-id", SwoopinApplication.settings().getIntegerValue(Settings.PREFKEY_USERID, 0).toString())
            }

            contentType(ContentType.Application.Json)
            body = jsonBody.toString()
        }

        // Check HTTP response first because payload could be invalid
        val responseString: String = response.receive()
        val httpResponseMsg = SwoopinAPI.validateHttpResponse(response)
        if (httpResponseMsg != SwoopinAPI.API_SUCCESS) {
            // Pass error message to caller for display
            resultCallback.invoke(httpResponseMsg)
            return
        }

        // Done eith async call
        refreshing = false

        // Validate JSON payload
        val responseObj = JSONObject(responseString)
        val responseMsg = SwoopinAPI.validateServerResponse(responseObj)
        if (responseMsg == SwoopinAPI.API_SUCCESS) {
            // Retrieve response payload
            parseUser(responseObj)
        }

        // Always invoke the callback
        resultCallback.invoke(responseMsg)
    }

    // ---------------------------------------------------------------------------------------
    //  parseUser - Parse User out of the JSON result payload
    // ---------------------------------------------------------------------------------------
    private fun parseUser(body: JSONObject)
    {
        try {
            // Get user
            val userInfo = UserInfo()
            val userObj: JSONObject = body.getJSONObject("response").getJSONObject("data").getJSONObject("user")
            userInfo.setValuesFromResponse(userObj)
            userMap[userInfo.id] = userInfo
        }
        catch (ex: JSONException) {
            ex.printStackTrace()
        }
    }
}
