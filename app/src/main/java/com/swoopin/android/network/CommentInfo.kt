package com.swoopin.android.network

import com.swoopin.android.utility.Utility
import org.json.JSONException
import org.json.JSONObject

// ---------------------------------------------------------------------------------------
//  CommentInfo - Comment Info. from an event
// ---------------------------------------------------------------------------------------
class CommentInfo
{
    var id = 0
    var eventId = 0
    var userId = 0
    var groupId = 0
    var voteCount = 0
    var userVoted = 0
    var comment = ""
    var addDttm = ""
    var lastupdateDttm = ""
    var fileId = 0
    var file: FileInfo? = null

    init {
        // Set default timestamp
        setTimeStamp()
    }

    // ---------------------------------------------------------------------------------------
    //  setTimeStamp - Set time stamp of object
    // ---------------------------------------------------------------------------------------
    fun setTimeStamp() {
        lastupdateDttm = addDttm
        addDttm = Utility.getTimeStamp(null)
    }

    // ---------------------------------------------------------------------------------------
    //  getFileName - Get name of specified file
    // ---------------------------------------------------------------------------------------
    fun getFileName(fileId: Int): String {
        var rc = ""
        if ((file != null) && (file!!.id == fileId))
            rc = file!!.fileName
        return rc
    }

    // ---------------------------------------------------------------------------------------
    //  JSON - Get object as JSON
    // ---------------------------------------------------------------------------------------
    val jSON: JSONObject
        get() {
            setTimeStamp()
            val obj = JSONObject()
            try {
                obj.put("id", id)
                obj.put("userId", userId)
                obj.put("eventId", eventId)
                obj.put("voteCount", voteCount)
                obj.put("userVoted", userVoted)
                obj.put("comment", comment)
                obj.put("addDttm", addDttm)
                obj.put("lastupdateDttm", lastupdateDttm)

                // Parse optional 'groupId' field
                if (groupId > 0)
                    obj.put("groupId", groupId)

                // Parse optional 'file' field
                if (file != null) {
                    obj.put("fileId", fileId)
                    obj.put("file", file)
                }

            } catch (e: JSONException) {
                e.printStackTrace()
            }
            return obj
        }

    // ---------------------------------------------------------------------------------------
    //  setValuesFromResponse - Set field values from an API response object
    // ---------------------------------------------------------------------------------------
    fun setValuesFromResponse(commentObj: JSONObject): CommentInfo
    {
        try
        {
            id = commentObj.optInt("id", id)
            userId = commentObj.optInt("userId", userId)
            eventId = commentObj.optInt("eventId", eventId)
            voteCount = commentObj.optInt("voteCount", voteCount)
            userVoted = commentObj.optInt("userVoted", userVoted)
            comment = commentObj.optString("comment", comment)
            addDttm = commentObj.optString("addDttm", addDttm)
            lastupdateDttm = commentObj.optString("lastupdateDttm", lastupdateDttm)

            // Parse optional 'groupId' field
            groupId = 0
            if (commentObj.has("groupId")) {
                groupId = commentObj.optInt("groupId", groupId)
            }

            // Parse optional 'file' field
            fileId = 0
            if (commentObj.has("file")) {
                val fileObj = commentObj.getJSONObject("file")
                file = FileInfo().setValuesFromResponse(fileObj)
                fileId = commentObj.optInt("fileId", fileId)
            }
        }
        catch (ex: JSONException)
        {
            ex.printStackTrace()
        }

        return this
    }
}
