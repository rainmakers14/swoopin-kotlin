package com.swoopin.android.network

import com.swoopin.android.utility.Utility
import org.json.JSONException
import org.json.JSONObject

// ---------------------------------------------------------------------------------------
//  UserInfo - Event info. from an Event
// ---------------------------------------------------------------------------------------
class UserInfo
{
    var id = 0
    var color = 0
    var score = 0
    var emailAddr = ""
    var emailGuid = ""
    var firstName = ""
    var lastName = ""
    var phoneNumber = ""
    var userType = ""
    var status = ""
    var hideProfile = 0
    var profileImageId = 0
    var verificationCode = 0
    var notificationMask = 0
    var addDttm = ""
    var lastupdateDttm = ""
    var lastLogonDttm = ""
    var file: FileInfo? = null

/*
    "profileImageId": 764,
    "myEvents": [
    "lastLogonDttm": "2021-10-14 15:40:31",
    "friendStatus": "NOTHING",
    "blocked": false,
    "msgs": []
*/

    init {
        // Set default timestamp
        setTimeStamp()
    }

    // ---------------------------------------------------------------------------------------
    //  setTimeStamp - Set time stamp of object
    // ---------------------------------------------------------------------------------------
    fun setTimeStamp() {
        lastupdateDttm = addDttm
        addDttm = Utility.getTimeStamp(null)
    }

    // ---------------------------------------------------------------------------------------
    //  getFileName - Get name of specified file
    // ---------------------------------------------------------------------------------------
    fun getFileName(fileId: Int): String {
        var rc = ""
        if ((file != null) && (file!!.id == fileId))
            rc = file!!.fileName
        return rc
    }

    // ---------------------------------------------------------------------------------------
    //  JSON - Get object as JSON
    // ---------------------------------------------------------------------------------------
    val jSON: JSONObject
        get()
        {
            setTimeStamp()
            val obj = JSONObject()
            try
            {
                // Parse normal fields
                obj.put("id", id)
                obj.put("color", color)
                obj.put("score", score)
                obj.put("emailAddr", emailAddr)
                obj.put("emailGuid", emailGuid)
                obj.put("firstName", firstName)
                obj.put("lastName", lastName)
                obj.put("phoneNumber", phoneNumber)
                obj.put("userType", userType)
                obj.put("status", status)
                obj.put("hideProfile", hideProfile)
                obj.put("profileImageId", profileImageId)
                obj.put("verificationCode", verificationCode)
                obj.put("notificationMask", notificationMask)
                obj.put("addDttm", addDttm)
                obj.put("lastupdateDttm", lastupdateDttm)
                obj.put("lastLogonDttm", lastLogonDttm)

                // Parse optional 'file' field
                if (file != null)
                    obj.put("file", file)
            }
            catch (e: JSONException)
            {
                e.printStackTrace()
            }

            return obj
        }

    // ---------------------------------------------------------------------------------------
    //  setValuesFromResponse - Set field values from an API response object
    // ---------------------------------------------------------------------------------------
    fun setValuesFromResponse(userObj: JSONObject): UserInfo
    {
        try
        {
            // Parse normal fields
            id = userObj.optInt("id", id)
            color = userObj.optInt("color", color)
            score = userObj.optInt("score", score)
            emailAddr = userObj.optString("emailAddr", emailAddr)
            emailGuid = userObj.optString("emailGuid", emailGuid)
            firstName = userObj.optString("firstName", firstName)
            lastName = userObj.optString("lastName", lastName)
            phoneNumber = userObj.optString("phoneNumber", phoneNumber)
            userType = userObj.optString("userType", userType)
            status = userObj.optString("status", status)
            hideProfile = userObj.optInt("hideProfile", hideProfile)
            profileImageId = userObj.optInt("profileImageId", profileImageId)
            verificationCode = userObj.optInt("verificationCode", verificationCode)
            notificationMask = userObj.optInt("notificationMask", notificationMask)
            addDttm = userObj.optString("addDttm", addDttm)
            lastupdateDttm = userObj.optString("lastupdateDttm", lastupdateDttm)
            lastLogonDttm= userObj.optString("", )

            // Parse optional 'file' field
            if (userObj.has("file")) {
                val fileObj = userObj.getJSONObject("file")
                file = FileInfo().setValuesFromResponse(fileObj)
            }
        }
        catch (ex: JSONException)
        {
            ex.printStackTrace()
        }

        return this
    }
}
