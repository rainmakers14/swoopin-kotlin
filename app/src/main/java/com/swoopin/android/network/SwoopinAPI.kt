package com.swoopin.android.network

// JSON stuff
import android.graphics.Bitmap
import org.json.JSONObject

// Android stuff
import android.widget.ImageView
import kotlin.random.Random

// Google Play stuff
import com.google.android.gms.maps.model.LatLng

// KTOR HTTP Client stuff
import io.ktor.http.*
import io.ktor.util.*
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.request.*
import io.ktor.client.features.*
import io.ktor.client.statement.*
import io.ktor.client.request.forms.*
import io.ktor.client.engine.android.*
import io.ktor.client.features.logging.*

// Swoopin Stuff
import com.swoopin.android.application.SwoopinApplication
import com.swoopin.android.utility.Settings
import com.swoopin.android.utility.Utility

// Third party stuff
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.Glide
import io.ktor.utils.io.core.*

// JSON references
// https://ktor.io/docs/response.html#json
// https://ktor.io/docs/json.html#receive_send_data
// https://medium.com/nerd-for-tech/json-serialization-for-ktor-feae3d06eadb

// Ktor references
// https://github.com/Kotlin/kotlinx.serialization/blob/master/docs/serialization-guide.md
// https://ktor.io/learn/
// https://ktor.io/docs/request.html
// https://ktor.io/docs/json.html#receive_send_data
// https://ktor.io/docs/request.html#form_parameters

// ---------------------------------------------------------------------------------------
//  SwoopinAPI - Singleton instance of the Swoopin API interface
// ---------------------------------------------------------------------------------------
object SwoopinAPI
{
    // Base URLs
    private const val LOGTAG = "SwoopinAPI"
    const val DATE_FORMAT = "yyyy-MM-dd HH:mm:ss"
    const val API_BASE_URL = "https://api-v3.swoopin.net/api/v3/"
    const val IMG_BASE_URL = "http://api-v3.swoopin.net/images/"

    // File URLs
    const val URLUploadFile = API_BASE_URL + "uploadFile.html"

    // Authentication URLs
    const val URLCreateProfile = API_BASE_URL + "createProfile.html"
    const val URLSaveProfile = API_BASE_URL + "saveProfile.html"
    const val URLAuthenticateProfile = API_BASE_URL + "authenticateProfile.html"
    const val URLAuthenticateToken = API_BASE_URL + "authenticateToken.html"
    const val URLForgotPassword = API_BASE_URL + "forgotPassword.html"

    // Event URLs
    const val URLGetEvents = API_BASE_URL + "getEvents.html"
    const val URLCreateEvent = API_BASE_URL + "createEvent.html"
    const val URLEditEvent = API_BASE_URL + "editEvent.html"
    const val URLShareEvent = API_BASE_URL + "shareEvent.html"
    const val URLFollowEvent = API_BASE_URL + "followEvent.html"
    const val URLUnFollowEvent = API_BASE_URL + "unfollowEvent.html"
    const val URLDeleteEvent = API_BASE_URL + "deleteEvent.html"
    const val URLRepeatEvent = API_BASE_URL + "repeatEvent.html"

    // User URLs
    const val URLGetUser = API_BASE_URL + "getUser.html"
    const val URLFindUsers = API_BASE_URL + "findUsers.html"
    const val URLGetFriends = API_BASE_URL + "getFriends.html"
    const val URSetAttribute = API_BASE_URL + "setAttribute.html"

    // Comment URLs
    const val URLCreateCommentImage = API_BASE_URL + "createCommentImage.html"
    const val URLCreateComment = API_BASE_URL + "createComment.html"
    const val URLDeleteComment = API_BASE_URL + "deleteComment.html"
    const val URLGetComments = API_BASE_URL + "getComments.html"
    const val URLDeleteVote = API_BASE_URL + "deleteVote.html"
    const val URLAddVote = API_BASE_URL + "addVote.html"

    // Success and failure codes for API calls
    const val API_FAIL = "FAIL"
    const val API_SUCCESS = "SUCCESS"

    // Event types
    val eventCategory = arrayListOf( "Social", "Service", "Sponsored" )

    // Local cache of server data
    val appInfo = AppInfo()
    val profileInfo = ProfileInfo()

    // Convenience variable
    val appContext = SwoopinApplication.instance.applicationContext

    // Current device location
    var deviceLocation: LatLng? = null

    // KTOR HTTP Client
    // https://ktor.io/docs/http-client-engines.html#okhttp
    val httpClient = getSwoopinHttpClient()

    // Do this in destructor?
//    httpClient.close()

    // ---------------------------------------------------------------------------------------
    //  init - Constructor
    // ---------------------------------------------------------------------------------------
//    init {
//    }

    // ---------------------------------------------------------------------------------------
    //  setDeviceLocation - Set current device location
    // ---------------------------------------------------------------------------------------
    fun setDeviceLocation(latitude: Double, longitude: Double)
    {
        // Set all instances of the value
        deviceLocation = LatLng(latitude, longitude)
        profileInfo.latitude = latitude.toString()
        profileInfo.longitude = longitude.toString()
        SwoopinApplication.settings().setStringValue(Settings.PREFKEY_LATITUDE, latitude.toString())
        SwoopinApplication.settings().setStringValue(Settings.PREFKEY_LONGITUDE, longitude.toString())

        // Update event data
        Events.refresh()
    }

    // ---------------------------------------------------------------------------------------
    //  getHttpClient - Create HTTP client
    // ---------------------------------------------------------------------------------------
    private fun getSwoopinHttpClient(): HttpClient
    {
        // Create regular or debugging client
        val client = HttpClient(Android) {
            // Disable default exception handling for 300, 400, 500 range responses
            expectSuccess = false

            install(Logging) {
                logger = Logger.ANDROID
                level = LogLevel.ALL
            }

        }

        return client
    }

    // ---------------------------------------------------------------------------------------
    //  createProfile - Create new profile based on provided credentials
    // ---------------------------------------------------------------------------------------
    suspend fun createProfile(firstName: String, lastName: String, email: String, password: String, resultCallback: (result: String) -> Unit)
    {
        // Split name into tokens or provide two different fields for user!!!

        // Save new values
        profileInfo.firstName = firstName
        profileInfo.lastName = lastName
        profileInfo.emailAddr = email
        profileInfo.password = password

        // Send only data needed for account creation (using entire profileInfo causes server to fail)
        val jsonProfile = JSONObject()
        jsonProfile.put("firstName", profileInfo.firstName)
        jsonProfile.put("lastName", profileInfo.lastName)
        jsonProfile.put("emailAddr", profileInfo.emailAddr)
        jsonProfile.put("password", profileInfo.password)

        // Create request JSON
        val jsonBody = JSONObject()
        val jsonRequest = JSONObject()
        jsonRequest.put("profile", jsonProfile)
        jsonBody.put("request", jsonRequest)

        // Create the HTTP request
        val response: HttpResponse = httpClient.post(URLCreateProfile) {
            headers {
                append(HttpHeaders.ContentType, "application/json")
                append(HttpHeaders.CacheControl, "no-cache")
            }

            contentType(ContentType.Application.Json)
            body = jsonBody.toString()
        }

        // Check HTTP response first because payload could be invalid
        val responseString: String = response.receive()
        val httpResponseMsg = validateHttpResponse(response)
        if (httpResponseMsg != API_SUCCESS) {
            // Pass error message to caller for display
            resultCallback.invoke(httpResponseMsg)
            return
        }

        // Validate JSON payload
        val responseObj = JSONObject(responseString)
        val responseMsg = validateServerResponse(responseObj)
        if (responseMsg != API_SUCCESS) {
            // Pass error message to caller for display
            resultCallback.invoke(responseMsg)
        }
        else {
            // Retrieve response payload
            profileInfo.setValuesFromResponse(responseObj)
            resultCallback.invoke(API_SUCCESS)
        }
    }

    // ---------------------------------------------------------------------------------------
    //  logIn - Log in the specified user
    // ---------------------------------------------------------------------------------------
    suspend fun logIn(email: String, password: String, resultCallback: (result: String) -> Unit)
    {
        // Save new values
        profileInfo.emailAddr = email
        profileInfo.password = password

        // Send only data needed for login (using entire profileInfo causes server to fail)
        val jsonProfile = JSONObject()
        jsonProfile.put("id", profileInfo.id)
        jsonProfile.put("emailAddr", profileInfo.emailAddr)
        jsonProfile.put("password", profileInfo.password)
        jsonProfile.put("deviceUuid", profileInfo.deviceUuid)
         jsonProfile.put("deviceType", profileInfo.deviceType)

        // Create request JSON
        val jsonBody = JSONObject()
        val jsonRequest = JSONObject()
        jsonRequest.put("profile", jsonProfile)
        jsonBody.put("request", jsonRequest)

        // Create the HTTP request
        val response: HttpResponse = httpClient.post(URLAuthenticateProfile) {
            headers {
                append(HttpHeaders.ContentType, "application/json")
                append(HttpHeaders.CacheControl, "no-cache")
            }

            contentType(ContentType.Application.Json)
            body = jsonBody.toString()
        }

        // Check HTTP response first because payload could be invalid
        val responseString: String = response.receive()
        val httpResponseMsg = validateHttpResponse(response)
        if (httpResponseMsg != API_SUCCESS) {
            // Pass error message to caller for display
            resultCallback.invoke(httpResponseMsg)
            return
        }

        // Validate JSON payload
        val responseObj = JSONObject(responseString)
        val responseMsg = validateServerResponse(responseObj)
        if (responseMsg != API_SUCCESS) {
            // Pass error message to caller for display
            resultCallback.invoke(responseMsg)
        }
        else {
            // Retrieve response payload
            profileInfo.setValuesFromResponse(responseObj)
            resultCallback.invoke(API_SUCCESS)
        }
    }

    // ---------------------------------------------------------------------------------------
    //  validateToken - Validate user with Token returned from successful log in
    // ---------------------------------------------------------------------------------------
    suspend fun validateToken(resultCallback: (result: String) -> Unit)
    {
        // Update profile info. values
        profileInfo.setTimeStamp()
        // iOS version sets coordinates here but we don't have permission yet

        // Create request JSON
        val jsonBody = JSONObject()
        val jsonRequest = JSONObject()
        jsonRequest.put("appinfo", appInfo.jSON)
        jsonRequest.put("profile", profileInfo.jSON)
        jsonBody.put("request", jsonRequest)

        val response: HttpResponse = httpClient.post(URLAuthenticateToken) {
            headers {
                append(HttpHeaders.ContentType, "application/json")
                append(HttpHeaders.CacheControl, "no-cache")
                append("x-swoopin-authtoken", SwoopinApplication.settings().getStringValue(Settings.PREFKEY_AUTHTOKEN, ""))
                append("x-swoopin-id", SwoopinApplication.settings().getIntegerValue(Settings.PREFKEY_USERID, 0).toString())
            }

            contentType(ContentType.Application.Json)
            body = jsonBody.toString()
        }

        // Check HTTP response first because payload could be invalid
        val responseString: String = response.receive()
        val httpResponseMsg = validateHttpResponse(response)
        if (httpResponseMsg != API_SUCCESS) {
            // Pass error message to caller for display
            resultCallback.invoke(httpResponseMsg)
            return
        }

        // Validate JSON payload
        val responseObj = JSONObject(responseString)
        val responseMsg = validateServerResponse(responseObj)
        if (responseMsg != API_SUCCESS) {
            // Pass error message to caller for display
            resultCallback.invoke(responseMsg)
        }
        else {
            // Retrieve response payload
            profileInfo.setValuesFromResponse(responseObj)
            resultCallback.invoke(API_SUCCESS)
        }
    }

    // ---------------------------------------------------------------------------------------
    //  followEvent - Add user to specified event
    // ---------------------------------------------------------------------------------------
    suspend fun followEvent(eventId: Int, unfollow: Boolean, resultCallback: (result: String) -> Unit)
    {
        // Update profile info. values
        profileInfo.setTimeStamp()

        // Create request JSON
        val jsonBody = JSONObject()
        val jsonRequest = JSONObject()
        jsonRequest.put("appinfo", appInfo.jSON)
        jsonRequest.put("profile", profileInfo.jSON)
        jsonRequest.put("eventId", eventId)
        jsonBody.put("request", jsonRequest)

        // Set appropriate URL
        var endpointUrl = URLFollowEvent
        if (unfollow)
            endpointUrl = URLUnFollowEvent

        val response: HttpResponse = httpClient.post(endpointUrl) {
            headers {
                append(HttpHeaders.ContentType, "application/json")
                append(HttpHeaders.CacheControl, "no-cache")
                append("x-swoopin-authtoken", SwoopinApplication.settings().getStringValue(Settings.PREFKEY_AUTHTOKEN, ""))
                append("x-swoopin-id", SwoopinApplication.settings().getIntegerValue(Settings.PREFKEY_USERID, 0).toString())
            }

            contentType(ContentType.Application.Json)
            body = jsonBody.toString()
        }

        // Check HTTP response first because payload could be invalid
        val responseString: String = response.receive()
        val httpResponseMsg = validateHttpResponse(response)
        if (httpResponseMsg != API_SUCCESS) {
            // Pass error message to caller for display
            resultCallback.invoke(httpResponseMsg)
            return
        }

        // Validate JSON payload
        val responseObj = JSONObject(responseString)
        val responseMsg = validateServerResponse(responseObj)
        resultCallback.invoke(responseMsg)
    }

    // ---------------------------------------------------------------------------------------
    //  voteComment - Vote or unvote the specified comment
    // ---------------------------------------------------------------------------------------
    suspend fun voteComment(eventId: Int, commentId: Int, voted: Int, resultCallback: (result: String) -> Unit)
    {
        // Update profile info. values
        profileInfo.setTimeStamp()

        // Create request JSON
        val jsonBody = JSONObject()
        val jsonRequest = JSONObject()
        jsonRequest.put("appinfo", appInfo.jSON)
        jsonRequest.put("profile", profileInfo.jSON)
        jsonRequest.put("commentId", commentId)
        jsonRequest.put("userVoted", commentId)
        jsonBody.put("request", jsonRequest)

        // Set appropriate URL
        var endpointUrl = URLDeleteVote
        if (voted > 0)
            endpointUrl = URLAddVote

        val response: HttpResponse = httpClient.post(endpointUrl) {
            headers {
                append(HttpHeaders.ContentType, "application/json")
                append(HttpHeaders.CacheControl, "no-cache")
                append("x-swoopin-authtoken", SwoopinApplication.settings().getStringValue(Settings.PREFKEY_AUTHTOKEN, ""))
                append("x-swoopin-id", SwoopinApplication.settings().getIntegerValue(Settings.PREFKEY_USERID, 0).toString())
            }

            contentType(ContentType.Application.Json)
            body = jsonBody.toString()
        }

        // Check HTTP response first because payload could be invalid
        val responseString: String = response.receive()
        val httpResponseMsg = validateHttpResponse(response)
        if (httpResponseMsg != API_SUCCESS) {
            // Pass error message to caller for display
            resultCallback.invoke(httpResponseMsg)
            return
        }

        // Validate JSON payload
        val responseObj = JSONObject(responseString)
        val responseMsg = validateServerResponse(responseObj)
        if (responseMsg == API_SUCCESS) {
            // Update local value without pulling from server
            val event = Events.eventMap[eventId]
            if (event != null) {
                val comments = event.comments
                val comment = comments[commentId]
                if (comment != null) {
                    comment.userVoted = voted
                    if (voted > 0) {
                        comment.voteCount++
                    }
                    else {
                        comment.voteCount--
                    }
                }
            }
        }

        resultCallback.invoke(responseMsg)
    }

    @InternalAPI
    suspend fun createCommentImage(eventId: Int, comment: String, srcBitmap: ByteArray, resultCallback: (result: String) -> Unit)
    {
        // Update profile info. values
        profileInfo.setTimeStamp()

        // Create comment JSON
        val commentObj = JSONObject()
        commentObj.put("id", 0)
        commentObj.put("eventId", eventId)
        commentObj.put("userId", profileInfo.id)
        commentObj.put("addDttm", profileInfo.addDttm)
        commentObj.put("lastupdateDttm", profileInfo.addDttm)
        commentObj.put("comment", comment)

        // Create request JSON
        val jsonBody = JSONObject()
        val jsonRequest = JSONObject()
        jsonRequest.put("appinfo", appInfo.jSON)
        jsonRequest.put("profile", profileInfo.jSON)
        jsonRequest.put("comment", commentObj)
        jsonBody.put("request", jsonRequest)

        // Create "boundary" part of HTTP header
        val num1 = Random.nextInt()
        val num2 = Random.nextInt()
        val contentTypeString = String.format("multipart/form-data; boundary=Boundary+%08X%08X", num1, num2)

        val response: HttpResponse = httpClient.post(URLCreateCommentImage) {
            headers {
                append("x-swoopin-authtoken", SwoopinApplication.settings().getStringValue(Settings.PREFKEY_AUTHTOKEN, ""))
                append("x-swoopin-id", SwoopinApplication.settings().getIntegerValue(Settings.PREFKEY_USERID, 0).toString())
            }

            body = MultiPartFormDataContent(
                formData {
                    appendInput(
                        key = "key", Headers.build {
                            append(HttpHeaders.ContentType, "image/jpg")
                            append(HttpHeaders.ContentDisposition, "name=\"files\"; filename=\"image.jpg\"")
                        },
                        size = srcBitmap.size.toLong())
                    { buildPacket { writeFully(srcBitmap) }}

                    append(FormPart("data", jsonBody.toString()))
                }
            )
        }

        // Check HTTP response first because payload could be invalid
        val responseString: String = response.receive()
        val httpResponseMsg = validateHttpResponse(response)
        if (httpResponseMsg != API_SUCCESS) {
            // Pass error message to caller for display
            resultCallback.invoke(httpResponseMsg)
            return
        }

        // Validate JSON payload
        val responseObj = JSONObject(responseString)
        val responseMsg = validateServerResponse(responseObj)
        if (responseMsg == API_SUCCESS)
            Events.parseCommentFromResponse(responseObj)
        resultCallback.invoke(responseMsg)
    }

    // ---------------------------------------------------------------------------------------
    //  createCommentWithImage - Create comment with image
    // ---------------------------------------------------------------------------------------
    @InternalAPI
    suspend fun createCommentWithImage(eventId: Int, comment: String, srcBitmap: ByteArray, resultCallback: (result: String) -> Unit)
    {
        // Update profile info. values
        profileInfo.setTimeStamp()

        // Create comment JSON
        val commentObj = JSONObject()
        commentObj.put("id", 0)
        commentObj.put("eventId", eventId)
        commentObj.put("userId", profileInfo.id)
        commentObj.put("addDttm", profileInfo.addDttm)
        commentObj.put("lastupdateDttm", profileInfo.addDttm)
        commentObj.put("comment", comment)

        // Create request JSON
        val jsonBody = JSONObject()
        val jsonRequest = JSONObject()
        jsonRequest.put("appinfo", appInfo.jSON)
        jsonRequest.put("profile", profileInfo.jSON)
        jsonRequest.put("comment", commentObj)
        jsonBody.put("request", jsonRequest)

        // Create "boundary" part of HTTP header
        val num1 = Random.nextInt()
        val num2 = Random.nextInt()
        val contentTypeString = String.format("multipart/form-data; boundary=Boundary+%08X%08X", num1, num2)

        // Create POST request
        val response: HttpResponse = httpClient.submitFormWithBinaryData(
            url = URLCreateCommentImage,
            formData = formData {
                append("data", jsonRequest.toString(), Headers.build {
                    append(HttpHeaders.ContentType, ContentType.Application.Json)
                })

                append("image", srcBitmap, Headers.build {
                    append(HttpHeaders.ContentType, "image/jpg")
                    append(HttpHeaders.ContentDisposition, "name=\"files\"; filename=\"image.jpg\"")
                })
            },

            block = {
                headers {
                    append("x-swoopin-authtoken", SwoopinApplication.settings().getStringValue(Settings.PREFKEY_AUTHTOKEN, ""))
                    append("x-swoopin-id", SwoopinApplication.settings().getIntegerValue(Settings.PREFKEY_USERID, 0).toString())
                }

//                contentType(ContentType.Application.Json)
//                body = jsonBody.toString()

//                method = HttpMethod.Post

                onUpload { bytesSentTotal, contentLength ->
                    Utility.logDebug("Sent $bytesSentTotal bytes from $contentLength")
                }
            })

        // Check HTTP response first because payload could be invalid
        val responseString: String = response.receive()
        val httpResponseMsg = validateHttpResponse(response)
        if (httpResponseMsg != API_SUCCESS) {
            // Pass error message to caller for display
            resultCallback.invoke(httpResponseMsg)
            return
        }

        // Validate JSON payload
        val responseObj = JSONObject(responseString)
        val responseMsg = validateServerResponse(responseObj)
        if (responseMsg == API_SUCCESS)
            Events.parseCommentFromResponse(responseObj)
        resultCallback.invoke(responseMsg)
    }

    // ---------------------------------------------------------------------------------------
    //  createComment - Add comment to specified event
    // ---------------------------------------------------------------------------------------
    suspend fun createComment(eventId: Int, comment: String, resultCallback: (result: String) -> Unit)
    {
        // Update profile info. values
        profileInfo.setTimeStamp()

        // Create comment JSON
        val commentObj = JSONObject()
        commentObj.put("id", 0)
        commentObj.put("eventId", eventId)
        commentObj.put("userId", profileInfo.id)
        commentObj.put("addDttm", profileInfo.addDttm)
        commentObj.put("lastupdateDttm", profileInfo.addDttm)
        commentObj.put("comment", comment)

        // Create request JSON
        val jsonBody = JSONObject()
        val jsonRequest = JSONObject()
        jsonRequest.put("appinfo", appInfo.jSON)
        jsonRequest.put("profile", profileInfo.jSON)
        jsonRequest.put("comment", commentObj)
        jsonBody.put("request", jsonRequest)

        val response: HttpResponse = httpClient.post(URLCreateComment) {
            headers {
                append(HttpHeaders.ContentType, "application/json")
                append(HttpHeaders.CacheControl, "no-cache")
                append("x-swoopin-authtoken", SwoopinApplication.settings().getStringValue(Settings.PREFKEY_AUTHTOKEN, ""))
                append("x-swoopin-id", SwoopinApplication.settings().getIntegerValue(Settings.PREFKEY_USERID, 0).toString())
            }

            contentType(ContentType.Application.Json)
            body = jsonBody.toString()
        }

        // Check HTTP response first because payload could be invalid
        val responseString: String = response.receive()
        val httpResponseMsg = validateHttpResponse(response)
        if (httpResponseMsg != API_SUCCESS) {
            // Pass error message to caller for display
            resultCallback.invoke(httpResponseMsg)
            return
        }

        // Validate JSON payload
        val responseObj = JSONObject(responseString)
        val responseMsg = validateServerResponse(responseObj)
        if (responseMsg == API_SUCCESS)
            Events.parseCommentFromResponse(responseObj)
        resultCallback.invoke(responseMsg)
    }

    // ---------------------------------------------------------------------------------------
    //  deleteComment - Delete specified comment
    // ---------------------------------------------------------------------------------------
    suspend fun deleteComment(eventId: Int, commentId: Int, resultCallback: (result: String) -> Unit)
    {
        // Update profile info. values
        profileInfo.setTimeStamp()

        // Create request JSON
        val jsonBody = JSONObject()
        val jsonRequest = JSONObject()
        jsonRequest.put("appinfo", appInfo.jSON)
        jsonRequest.put("profile", profileInfo.jSON)
        jsonRequest.put("commentId", commentId)
        jsonBody.put("request", jsonRequest)

        val response: HttpResponse = httpClient.post(URLDeleteComment) {
            headers {
                append(HttpHeaders.ContentType, "application/json")
                append(HttpHeaders.CacheControl, "no-cache")
                append("x-swoopin-authtoken", SwoopinApplication.settings().getStringValue(Settings.PREFKEY_AUTHTOKEN, ""))
                append("x-swoopin-id", SwoopinApplication.settings().getIntegerValue(Settings.PREFKEY_USERID, 0).toString())
            }

            contentType(ContentType.Application.Json)
            body = jsonBody.toString()
        }

        // Check HTTP response first because payload could be invalid
        val responseString: String = response.receive()
        val httpResponseMsg = validateHttpResponse(response)
        if (httpResponseMsg != API_SUCCESS) {
            // Pass error message to caller for display
            resultCallback.invoke(httpResponseMsg)
            return
        }

        // Validate JSON payload
        val responseObj = JSONObject(responseString)
        val responseMsg = validateServerResponse(responseObj)
        if (responseMsg == API_SUCCESS)
            Events.deleteCommentLocal(eventId, commentId) // Fake it so we don't have to retrieve events
        resultCallback.invoke(responseMsg)
    }

    // ---------------------------------------------------------------------------------------
    //  validateHttpResponse - Check for errors in HTTP responses
    // ---------------------------------------------------------------------------------------
    fun validateHttpResponse(response: HttpResponse): String
    {
        // Check for HTTP error codes
        if (response.status.value < 200 || response.status.value > 299)
        {
            // Set different error message based on HTTP code?
            val errorMsg = "Server connection issue. HTTP Response: [" + response.status.value + "], " + response.status.description
            return errorMsg
        }

        // At this point we know the call succeeded
        return API_SUCCESS
    }

    // ---------------------------------------------------------------------------------------
    //  validateServerResponse - Check for errors from the SwoopIn Server
    // ---------------------------------------------------------------------------------------
    fun validateServerResponse(responseObj: JSONObject): String
    {
        // Check for SwoopIn 'FAIL' status
        val rsp: JSONObject = responseObj.getJSONObject("response")
        val status: String = rsp.optString("status", API_FAIL)
        if (status == API_FAIL) {
            val msgArray = rsp.getJSONArray("msgs")
            val errorMsg = msgArray.getJSONObject(0)
            if (errorMsg != null)
                return errorMsg.optString("error", "API call failed for unknown reason.")
        }

        // At this point we know the call succeeded
        return API_SUCCESS
    }

    // ---------------------------------------------------------------------------------------
    //  loadImage - Load image directly into an ImageView
    //     https://bumptech.github.io/glide/
    // ---------------------------------------------------------------------------------------
    fun loadImage(fileName: String, imageView: ImageView)
    {
        // Safety first
        if (fileName.isEmpty())
            return

        val imageUri = IMG_BASE_URL + fileName
        Glide.with(appContext)
            .load(imageUri)
            .into(imageView);
    }

    // ---------------------------------------------------------------------------------------
    //  loadCircularImage - Load circular image asynchronously
    // ---------------------------------------------------------------------------------------
    fun loadCircularImage(fileName: String, imageView: ImageView)
    {
        // Safety first
        if (fileName.isEmpty())
            return

        val imageUri = IMG_BASE_URL + fileName
        Glide.with(appContext)
            .load(imageUri)
            .circleCrop()
            .into(imageView)
    }

    // ---------------------------------------------------------------------------------------
    //  loadBitmap - Load image into an off-screen Bitmap
    // ---------------------------------------------------------------------------------------
    fun loadBitmap(fileName: String, bitmap: CustomTarget<Bitmap>)
    {
        // Safety first
        if (fileName.isEmpty())
            return

        val imageUri = IMG_BASE_URL + fileName
        Glide.with(appContext)
            .asBitmap()
            .load(imageUri)
            .dontAnimate()
            .into(bitmap)
    }

    // ---------------------------------------------------------------------------------------
    //  loadCircularBitmap - Load image into an off-screen Bitmap with circle mask
    // ---------------------------------------------------------------------------------------
    fun loadCircularBitmap(fileName: String, bitmap: CustomTarget<Bitmap>)
    {
        // Safety first
        if (fileName.isEmpty())
            return

        val imageUri = IMG_BASE_URL + fileName
        Glide.with(appContext)
            .asBitmap()
            .load(imageUri)
            .circleCrop()
            .dontAnimate()
            .into(bitmap)
    }
}
