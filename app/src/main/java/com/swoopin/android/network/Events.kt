package com.swoopin.android.network

// Java stuff
import java.util.*
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

// JSON stuff
import org.json.JSONException
import org.json.JSONObject
import org.json.JSONArray

// Android stuff
import kotlinx.coroutines.runBlocking
import android.graphics.BitmapFactory
import android.graphics.Bitmap

// Ktor stuff
import io.ktor.http.*
import io.ktor.client.call.*
import io.ktor.client.request.*
import io.ktor.client.statement.*

// Swoopin stuff
import com.swoopin.android.application.SwoopinApplication
import com.swoopin.android.utility.Settings
import com.swoopin.android.utility.Utility
import java.time.format.FormatStyle
import com.swoopin.android.R

// ---------------------------------------------------------------------------------------
//  Events - Singleton instance of all Swoopin API Event data
// ---------------------------------------------------------------------------------------
object Events
{
    // Event search parameters
    val MAX_DAYS_AHEAD = 7
    var currentDaysAhead = MAX_DAYS_AHEAD

    // Refresh event listeners
    interface EventCallback {
        fun onEventsRefreshed()
    }

    // True while refreshing event list
    private var refreshing = false

    // Current list of events
    val eventMap = hashMapOf<Int, EventInfo>()

    // List of subscribers to refresh events
    private val refreshListeners: MutableSet<EventCallback> = mutableSetOf()

    // ---------------------------------------------------------------------------------------
    //  getEventsByStart - Get list of events ordered by starting time
    // ---------------------------------------------------------------------------------------
    fun getEventsByStart(): MutableList<EventInfo>
    {
        // https://howtodoinjava.com/java/date-time/parse-string-to-date-time-utc-gmt/

        // The event list (empty if no events available)
        val eventList = mutableListOf<EventInfo>()
        if (!refreshing)
        {
            // Loop over all events
            eventMap.forEach { (_, value) ->
                // Extract event
                val event: EventInfo = value

                // Insert first one
                if (eventList.size <= 0) {
                    eventList.add(value)
                    return@forEach  // Same as "continue" in a loop
                }

                // Extract start date of new event
                val newStart = Utility.getLocalDateTime(event.startDttm)

                // Determine where to insert
                var added = false
                for (i in eventList.indices) {
                    val oldStart = Utility.getLocalDateTime(eventList[i].startDttm)
                    if (newStart.isBefore(oldStart)) {
                        eventList.add(i, event)
                        added = true
                        break
                    }
                }

                // If it wasn't added then insert it at the end
                if (!added)
                    eventList.add(eventList.size, event)
            }
        }

        return eventList
    }

    // ---------------------------------------------------------------------------------------
    //  getEventsByDistance - Get list of events ordered by distance
    // ---------------------------------------------------------------------------------------
    fun getEventsByDistance(): MutableList<EventInfo>
    {
        // The event list (empty if no events available)
        val eventList = mutableListOf<EventInfo>()
        if (!refreshing)
        {
            eventMap.forEach { (_, value) ->
                val event: EventInfo = value

                // Insert at end for now
                eventList.add(value)
            }
        }

        return eventList
    }

    // ---------------------------------------------------------------------------------------
    //  getEventsByNew - Get list of events ordered by recent creation
    // ---------------------------------------------------------------------------------------
    fun getEventsByNew(): MutableList<EventInfo>
    {
        // The event list (empty if no events available)
        val eventList = mutableListOf<EventInfo>()
        if (!refreshing)
        {
            eventMap.forEach { (_, value) ->
                val event: EventInfo = value

                // Insert at end for now
                eventList.add(value)
            }
        }

        return eventList
    }

    // ---------------------------------------------------------------------------------------
    //  timeToEvent - Calculate time from now until event starts
    // ---------------------------------------------------------------------------------------
    fun timeToEvent(eventId: Int): String
    {
        // Set defaults
        var daysUntil = 0L
        var hoursUntil = 0L
        var minutesUntil = 0L

        // Check for valid event
        val event = eventMap[eventId]
        if (event != null)
        {
            // Get the time diff
            val timeDiff = Utility.getTimeUntil(event.startDttm)
            daysUntil = timeDiff.toDays()
            hoursUntil = timeDiff.toHours()
            minutesUntil = timeDiff.toMinutes()
        }

        // Check larger denominations first
        if (daysUntil > 0)
            return String.format(SwoopinApplication.instance.getString(R.string.fmt_num_days), daysUntil)
        else if (hoursUntil > 0)
            return String.format(SwoopinApplication.instance.getString(R.string.fmt_num_hours), hoursUntil)
        else if (minutesUntil > 0)
            return String.format(SwoopinApplication.instance.getString(R.string.fmt_num_minutes), minutesUntil)
        else
            return SwoopinApplication.instance.getString(R.string.fmt_now)
    }

    // ---------------------------------------------------------------------------------------
    //  getLocation - Get event location
    // ---------------------------------------------------------------------------------------
    fun getLocation(evId: Int): String
    {
        var rc = "--"
        val event = eventMap[evId]
        if (event != null)
            rc = event.location
        return rc
    }

    // ---------------------------------------------------------------------------------------
    //  getDistance - Helper function to get distance to event
    // ---------------------------------------------------------------------------------------
    fun getDistance(evId: Int): Double
    {
        var rc = 0.0
        val event = eventMap[evId]
        if (event != null)
            rc = event.distance.toDouble()
        return rc
    }

    // ---------------------------------------------------------------------------------------
    //  formatDistance - Helper function to shorten distance for UI display
    // ---------------------------------------------------------------------------------------
    fun formatDistance(evId: Int): String
    {
        var rc = "--"
        val event = eventMap[evId]
        if (event != null)
            rc = String.format(SwoopinApplication.instance.getString(R.string.fmt_distance), event.distance.toDouble())
        return rc
    }

    // ---------------------------------------------------------------------------------------
    //  getStartTime - Get the event start time
    // ---------------------------------------------------------------------------------------
    fun getStartTime(eventId: Int): LocalDateTime?
    {
        var rc: LocalDateTime? = null

        // Extract event start date
        val event = eventMap[eventId]
        if (event != null)
            rc = Utility.getLocalDateTime(event.startDttm)

        return rc
    }

    // ---------------------------------------------------------------------------------------
    //  getEndTime - Get the event end time
    // ---------------------------------------------------------------------------------------
    fun getEndTime(eventId: Int): LocalDateTime?
    {
        var rc: LocalDateTime? = null

        // Extract event start date
        val event = eventMap[eventId]
        if (event != null)
            rc = Utility.getLocalDateTime(event.endDttm)

        return rc
    }

    // ---------------------------------------------------------------------------------------
    //  formatStartTime - Get start time as a string formatted for display
    // ---------------------------------------------------------------------------------------
    fun formatStartTime(eventId: Int): String
    {
        var rc = "--/--/--"
        val startTime = getStartTime(eventId)
        if (startTime != null) {
            val date = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM).format(startTime)
            val time = DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT).format(startTime)
            rc = "$date $time"
        }

        return rc
    }

    // ---------------------------------------------------------------------------------------
    //  formatEndTime - Get end time as a string formatted for display
    // ---------------------------------------------------------------------------------------
    fun formatEndTime(eventId: Int): String
    {
        var rc = "--/--/--"
        val endTime = getEndTime(eventId)
        if (endTime != null) {
            val date = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM).format(endTime)
            val time = DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT).format(endTime)
            rc = "$date $time"
        }

        return rc
    }

    // ---------------------------------------------------------------------------------------
    //  numComments - Helper function to get number of comments of an event
    // ---------------------------------------------------------------------------------------
    fun numComments(evId: Int): Int
    {
        var rc = 0
        val event = eventMap[evId]
        if (event != null)
            rc = event.comments.size
        return rc
    }

    // ---------------------------------------------------------------------------------------
    //  numAttending - Helper function to get number of attendees of an event
    // ---------------------------------------------------------------------------------------
    fun numAttending(evId: Int): Int
    {
        var rc = 0
        val event = eventMap[evId]
        if (event != null)
            rc = event.userFollowers.size
        return rc
    }

    // ---------------------------------------------------------------------------------------
    //  isAttending - Check if specified user is attending specified event
    // ---------------------------------------------------------------------------------------
    fun isAttending(userId: Int, evId: Int): Boolean
    {
        var rc = false
        val event = eventMap[evId]
        if (event != null)
        {
            // Check creator
            if (event.creatorId == userId) {
                rc = true
            }
            else {
                // Check attendees
                for (i in 0 until event.userFollowers.size) {
                    val user = event.userFollowers[i]
                    if (user?.id == userId) {
                        rc = true
                        break
                    }
                }
            }
        }

        return rc
    }

    // ---------------------------------------------------------------------------------------
    //  getTitle - Get title of specified event
    // ---------------------------------------------------------------------------------------
    fun getTitle(eventId: Int): String
    {
        var title = ""
        val event = eventMap[eventId]
        if (event != null)
            title = event.eventActivity
        return title
    }

    // ---------------------------------------------------------------------------------------
    //  getDesc - Get description of specified event
    // ---------------------------------------------------------------------------------------
    fun getDesc(eventId: Int): String
    {
        var desc = ""
        val event = eventMap[eventId]
        if (event != null)
            desc = event.eventDesc
        return desc
    }

    // ---------------------------------------------------------------------------------------
    //  getTopImage - Determine top image to display for event
    // ---------------------------------------------------------------------------------------
    fun getTopImage(eventId: Int): String
    {
        var imageName = ""
        val event = eventMap[eventId]
        if (event != null)
        {
            // Find filename associated with this fileID
            val topId = event.topImageId
            event.comments.forEach { (_, value) ->
                // Extract comment
                val comment: CommentInfo = value
                if (comment.fileId == topId) {
                    imageName = comment.getFileName(topId)
                    return@forEach
                }
            }
        }

        return imageName
    }

    // ---------------------------------------------------------------------------------------
    //  getCreatorName - Get first name of creator
    // ---------------------------------------------------------------------------------------
    fun getCreatorName(eventId: Int): String
    {
        var creatorName = ""
        val event = eventMap[eventId]
        if (event != null)
        {
            if ((event.creatorType  == "USER") && (event.user != null))
            {
                if (event.user!!.id == event.creatorId)
                    creatorName = event.user!!.firstName
            }
            else if ((event.creatorType  == "GROUP") && (event.group != null))
            {
                if (event.group!!.id == event.creatorId)
                    creatorName = event.group!!.groupName
            }
        }

        return creatorName
    }

    // ---------------------------------------------------------------------------------------
    //  getCreatorImage - Determine creator image to display for event
    // ---------------------------------------------------------------------------------------
    fun getCreatorImage(eventId: Int): String
    {
        var imageName = ""
        val event = eventMap[eventId]
        if (event != null)
        {
            if ((event.creatorType  == "USER") && (event.user != null))
            {
                if ((event.user!!.id == event.creatorId) && (event.user!!.file != null))
                    imageName = event.user!!.file!!.fileName
            }
            else if ((event.creatorType  == "GROUP") && (event.group != null))
            {
                if ((event.group!!.id == event.creatorId) && (event.group!!.file != null))
                    imageName = event.group!!.file!!.fileName
            }
        }

        return imageName
    }

    // ---------------------------------------------------------------------------------------
    //  getEventIcon - Determine which image to use for the icon
    // ---------------------------------------------------------------------------------------
    fun getEventIcon(eventId: Int): Bitmap?
    {
        // Default to nothing
        var rc: Bitmap? = null

        // Make sure event exists
        val event = eventMap[eventId]
        if (event != null)
        {
            // Determine if there's an emoji
            if (!event.icon.isEmpty())
            {
                rc = Utility.getEmojiBitmap(event.icon, SwoopinAPI.appContext)
            }
            else
            {
                // Determine default pin
                val pins = arrayListOf(R.mipmap.ic_pin_purple, R.mipmap.ic_pin_green, R.mipmap.ic_pin_blue)
                for (i in SwoopinAPI.eventCategory.indices) {
                    if (event.category == SwoopinAPI.eventCategory[i]) {
                        val icon = pins[i]
                        rc = BitmapFactory.decodeResource(SwoopinAPI.appContext.resources, icon);
                        break
                    }
                }
            }
        }

        return rc
    }

    // ---------------------------------------------------------------------------------------
    //  getCommentAuthorName - Get name of comment author
    // ---------------------------------------------------------------------------------------
    fun getCommentAuthorName(comment: CommentInfo): String
    {
        // Default return value
        var author = ""

        // Get event containing comment
        val event = eventMap[comment.eventId]
        if (event != null)
        {
            // Check if comment was by group or user
            if (comment.groupId > 0)
            {
                // Check creator group first
                var group = event.group
                if ((group == null) || (group.id != comment.groupId))
                    group = event.groupFollowers[comment.groupId]

                // Extract name from chosen group
                if (group != null)
                    author = group.groupName
                else
                    author = "Invalid Group ID: ${comment.groupId}"
            }
            else
            {
                // Check creator first
                var user = event.user
                if ((user == null) || (user.id != comment.userId))
                    user = event.userFollowers[comment.userId]

                // Extract name from chosen user
                if (user != null)
                    author = user.firstName
                else
                    author = Users.getName(comment.userId)
            }
        }

        return author
    }

    // ---------------------------------------------------------------------------------------
    //  getCommentAuthorIcon - Get icon file name of comment author
    // ---------------------------------------------------------------------------------------
    fun getCommentAuthorIcon(comment: CommentInfo): String
    {
        // Default return value
        var icon = ""

        // Get event containing comment
        val event = eventMap[comment.eventId]
        if (event != null)
        {
            // Check if comment was by group or user
            if (comment.groupId > 0)
            {
                // Check creator group first
                var group = event.group
                if ((group == null) || (group.id != comment.groupId))
                    group = event.groupFollowers[comment.groupId]

                // Extract name from chosen group
                if (group != null) {
                    val file = group.file
                    if (file != null)
                        icon = file.fileName
                }
            }
            else
            {
                // Check creator first
                var user = event.user
                if ((user == null) || (user.id != comment.userId))
                    user = event.userFollowers[comment.userId]

                // Extract name from chosen user
                if ((user != null) && (user.file != null)) {
                    icon = user.file!!.fileName
                }
                else {
                    icon = Users.getIcon(comment.userId)
                }
            }
        }

        return icon
    }

    // ---------------------------------------------------------------------------------------
    //  getCommentVoted - Get voted status of comment
    // ---------------------------------------------------------------------------------------
    fun getCommentVoted(comment: CommentInfo): Boolean
    {
        // Default return value
        var voted = false
        if (comment.userVoted > 0)
            voted = true
        return voted
    }

    // ---------------------------------------------------------------------------------------
    //  getCommentImage - Get file name of comment image
    // ---------------------------------------------------------------------------------------
    fun getCommentImage(comment: CommentInfo): String
    {
        // Default return value
        var image = ""
        if (comment.file != null)
            image = comment.file!!.fileName
        return image
    }

    // ---------------------------------------------------------------------------------------
    //  timeSinceComment - Get difference between now and time when comment was posted
    // ---------------------------------------------------------------------------------------
    fun timeSinceComment(comment: CommentInfo): String
    {
        // Get the time diff
        val timeDiff = Utility.getTimeSince(comment.addDttm)
        val daysSince = timeDiff.toDays()
        val hoursSince = timeDiff.toHours()
        val minutesSince = timeDiff.toMinutes()

        // Check larger denominations first
        if (daysSince > 0)
            return String.format(SwoopinApplication.instance.getString(R.string.fmt_num_days), daysSince)
        else if (hoursSince > 0)
            return String.format(SwoopinApplication.instance.getString(R.string.fmt_num_hours), hoursSince)
        else
            return String.format(SwoopinApplication.instance.getString(R.string.fmt_num_minutes), minutesSince)
    }

    // ---------------------------------------------------------------------------------------
    //  deleteCommentLocal - Delete local copy of comment
    // ---------------------------------------------------------------------------------------
    fun deleteCommentLocal(evId: Int, commId: Int)
    {
        val event = eventMap[evId]
        event?.comments?.remove(commId)
    }

    // ---------------------------------------------------------------------------------------
    //  getComments - Get list of comments in reverse-chronological order (the default)
    // ---------------------------------------------------------------------------------------
    fun getComments(evId: Int): MutableList<CommentInfo>
    {
        // The comment list (empty if no events available)
        val commentList = mutableListOf<CommentInfo>()
        if (!refreshing)
        {
            // Loop over all comments
            val event = eventMap[evId]
            if (event != null)
            {
                // Loop over all comments
                event.comments.forEach { (_, value) ->
                    // Extract comment
                    val comment: CommentInfo = value

                    // Insert first one
                    var added = false
                    if (commentList.size <= 0)
                    {
                        commentList.add(comment)
                        added = true
                    }
                    else
                    {
                        // Extract date of comment posting
                        val postedTime = Utility.getLocalDateTime(comment.addDttm)

                        // Determine where to insert
                        for (i in commentList.indices) {
                            val listTime = Utility.getLocalDateTime(commentList[i].addDttm)
                            if (postedTime.isAfter(listTime)) {
                                commentList.add(i, comment)
                                added = true
                                break
                            }
                        }
                    }

                    // If it wasn't added then insert it at the end
                    if (!added)
                        commentList.add(commentList.size, comment)
                }
            }
        }

        return commentList
    }

    // ---------------------------------------------------------------------------------------
    //  subscribeRefreshEvents - Subscribe to know when events have been updated
    // ---------------------------------------------------------------------------------------
    fun subscribeRefreshEvents(listener: EventCallback) {
        refreshListeners += listener
    }

    // ---------------------------------------------------------------------------------------
    //  unsubscribeRefreshEvents - Subscribe to know when events have been updated
    // ---------------------------------------------------------------------------------------
    fun unsubscribeRefreshEvents(listener: EventCallback) {
        refreshListeners -= listener
    }

    // ---------------------------------------------------------------------------------------
    //  unsubscribeRefreshEvents - Subscribe to know when events have been updated
    // ---------------------------------------------------------------------------------------
    fun onEventsRefreshed() {
        // Notify every subscriber
        for (callback in refreshListeners) {
            callback.onEventsRefreshed()
        }
    }

    // ---------------------------------------------------------------------------------------
    //  refresh - Refresh local list of events, after location change or backgrounding
    // ---------------------------------------------------------------------------------------
    fun refresh()
    {
        // Request new Event data
        refreshing = true
        runBlocking {
            getEvents() { result ->
                // Async results arrived
                if (result != SwoopinAPI.API_SUCCESS) {
                    // Log error message
                    val errStr = "Failed to refresh events: $result"
                    Utility.logDebug(errStr)
                }
            }
        }
    }

    // ---------------------------------------------------------------------------------------
    //  getEvents - Get all relevant events based on current device location
    // ---------------------------------------------------------------------------------------
    private suspend fun getEvents(resultCallback: (result: String) -> Unit)
    {
        // Update profile info. values
        SwoopinAPI.profileInfo.setTimeStamp()

        // Create request JSON
        val jsonBody = JSONObject()
        val jsonRequest = JSONObject()

        // Create the Filter block
        val jsonFilter = JSONObject()
        jsonFilter.put("orderBy", "SOON")

        // Set time range to look for events
        val startDate = Utility.getNow()
        val endDate = startDate.plusDays(currentDaysAhead.toLong())

        // Convert dates to strings
        val startTime = Utility.getTimeStamp(startDate)
        val endTime = Utility.getTimeStamp(endDate)
        val addDttm: String = startTime

        // Create the Event block
        val jsonEvent = JSONObject()
        jsonEvent.put("id", 0)
        jsonEvent.put("addDttm", addDttm)
        jsonEvent.put("lastUpdateDttm", addDttm)
        jsonEvent.put("startDttm", startTime)
        jsonEvent.put("endDttm", endTime)
        jsonEvent.put("latitude", SwoopinAPI.profileInfo.latitude)
        jsonEvent.put("longitude", SwoopinAPI.profileInfo.longitude)

        // Create the REQUEST block
        jsonRequest.put("appinfo", SwoopinAPI.appInfo.jSON)
        jsonRequest.put("profile", SwoopinAPI.profileInfo.jSON)
        jsonRequest.put("filter", jsonFilter)
        jsonRequest.put("event", jsonEvent)
        jsonBody.put("request", jsonRequest)

        // Send request
        val response: HttpResponse = SwoopinAPI.httpClient.post(SwoopinAPI.URLGetEvents) {
            headers {
                append(HttpHeaders.ContentType, "application/json")
                append(HttpHeaders.CacheControl, "no-cache")
                append("x-swoopin-authtoken", SwoopinApplication.settings().getStringValue(Settings.PREFKEY_AUTHTOKEN, ""))
                append("x-swoopin-id", SwoopinApplication.settings().getIntegerValue(Settings.PREFKEY_USERID, 0).toString())
            }

            contentType(ContentType.Application.Json)
            body = jsonBody.toString()
        }

        // Check HTTP response first because payload could be invalid
        val responseString: String = response.receive()
        val httpResponseMsg = SwoopinAPI.validateHttpResponse(response)
        if (httpResponseMsg != SwoopinAPI.API_SUCCESS) {
            // Pass error message to caller for display
            resultCallback.invoke(httpResponseMsg)
            return
        }

        // Done with async call
        refreshing = false

        // Validate JSON payload
        val responseObj = JSONObject(responseString)
        val responseMsg = SwoopinAPI.validateServerResponse(responseObj)
        if (responseMsg == SwoopinAPI.API_SUCCESS) {
            // Retrieve response payload
            parseEvents(responseObj)
            // Apparently this message doesn't return a 'profile' object
//            SwoopinAPI.profileInfo.setValuesFromResponse(responseObj)
            onEventsRefreshed()
        }

        // Always invoke the callback
        resultCallback.invoke(responseMsg)
    }

    // ---------------------------------------------------------------------------------------
    //  parseEvents - Parse events out of the JSON result payload
    // ---------------------------------------------------------------------------------------
    private fun parseEvents(body: JSONObject)
    {
        try
        {
            // Get data for each event
            val events: JSONArray = body.getJSONObject("response").getJSONObject("data").getJSONArray("events")
            val numEvents = events.length()
            eventMap.clear()
            for (idx in 0 until numEvents)
            {
                // Parse event
                val eventObj = events.getJSONObject(idx)
                val evInfo = EventInfo()
                evInfo.setValuesFromResponse(eventObj)
                eventMap[evInfo.id] = evInfo
            }
        }
        catch (ex: JSONException)
        {
            ex.printStackTrace()
        }
    }

    // ---------------------------------------------------------------------------------------
    //  parseCommentFromResponse - Set specified comment from JSON response payload
    // ---------------------------------------------------------------------------------------
    fun parseCommentFromResponse(responseObj: JSONObject)
    {
        // Parse updated comment
        try {
            // Get comment object
            val cmtObj: JSONObject = responseObj.getJSONObject("response").getJSONObject("data").getJSONObject("comment")
            val commentInfo = CommentInfo()
            commentInfo.setValuesFromResponse(cmtObj)

            // Update event with new comment
            val event = eventMap[commentInfo.eventId]
            if (event != null)
                event.comments[commentInfo.id] = commentInfo
        }
        catch (ex: JSONException) {
            ex.printStackTrace()
        }
    }
}
