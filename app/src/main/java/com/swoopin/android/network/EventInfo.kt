package com.swoopin.android.network

import com.google.android.gms.maps.model.LatLng
import com.swoopin.android.utility.Utility
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

// ---------------------------------------------------------------------------------------
//  EventInfo - Event info. from an Event
// ---------------------------------------------------------------------------------------
class EventInfo
{
    var id = 0
    var category = ""
    var creatorId = 0
    var creatorType = ""
    var latitude = "0.0"
    var longitude = "0.0"
    var distance = "0.0"
    var status = ""
    var addDttm = ""
    var lastupdateDttm = ""
    var startDttm = ""
    var endDttm = ""
    var eventActivity = ""
    var eventDesc = ""
    var icon = ""
    var topImageId = 0
    var location = ""
    var voteCount = 0
    var user: UserInfo? = null
    var group: GroupInfo? = null
    val comments = hashMapOf<Int, CommentInfo>()
    val userFollowers = hashMapOf<Int, UserInfo>()
    val groupFollowers = hashMapOf<Int, GroupInfo>()

    init
    {
        // Set default timestamp
        setTimeStamp()
    }

    // ---------------------------------------------------------------------------------------
    //  setTimeStamp - Set time stamp of object
    // ---------------------------------------------------------------------------------------
    private fun setTimeStamp() {
        lastupdateDttm = addDttm
        addDttm = Utility.getTimeStamp(null)
    }

    // ---------------------------------------------------------------------------------------
    //  getLatLng - Get LatLng value
    // ---------------------------------------------------------------------------------------
    fun getLatLng(): LatLng {
        return LatLng(latitude.toDouble(), longitude.toDouble())
    }

    // ---------------------------------------------------------------------------------------
    //  JSON - Get object as JSON
    // ---------------------------------------------------------------------------------------
    val jSON: JSONObject
        get()
        {
            setTimeStamp()
            val obj = JSONObject()
            try
            {
                // Parse normal fields
                obj.put("id", id)
                obj.put("category", category)
                obj.put("creatorId", creatorId)
                obj.put("creatorType", creatorType)
                obj.put("latitude", latitude)
                obj.put("longitude", longitude)
                obj.put("distance", distance)
                obj.put("status", status)
                obj.put("eventActivity", eventActivity)
                obj.put("eventDesc", eventDesc)
                obj.put("icon", icon)
                obj.put("topImageId", topImageId)
                obj.put("location", location)
                obj.put("voteCount", voteCount)
                obj.put("addDttm", addDttm)
                obj.put("lastupdateDttm", lastupdateDttm)
                obj.put("startDttm", startDttm)
                obj.put("endDttm", endDttm)

                // Parse optional fields
                if (user != null)
                    obj.put("user", user)
                if (group != null)
                    obj.put("group", group)

                // Parse comments
                val commentArray = JSONArray()
                comments.forEach { (_, value) ->
                    val commentObj: CommentInfo = value
                    commentArray.put(commentObj)
                }
                obj.put("comments", commentArray)

                // Parse user followers
                val userArray = JSONArray()
                userFollowers.forEach { (_, value) ->
                    val userObj: UserInfo = value
                    userArray.put(userObj)
                }
                obj.put("userFollowers", userArray)

                // Parse group followers
                val groupArray = JSONArray()
                groupFollowers.forEach { (_, value) ->
                    val groupObj: GroupInfo = value
                    groupArray.put(groupObj)
                }
                obj.put("groupFollowers", groupArray)
            }
            catch (e: JSONException)
            {
                e.printStackTrace()
            }

            return obj
        }

    // ---------------------------------------------------------------------------------------
    //  setValuesFromResponse - Set field values from an API response object
    // ---------------------------------------------------------------------------------------
    fun setValuesFromResponse(eventObj: JSONObject)
    {
        try
        {
            // Parse normal fields
            id = eventObj.optInt("id", id)
            creatorId = eventObj.optInt("creatorId", creatorId)
            category = eventObj.optString("category", category)
            creatorType = eventObj.optString("creatorType", creatorType)
            latitude = eventObj.optString("latitude", latitude)
            longitude = eventObj.optString("longitude", longitude)
            distance = eventObj.optString("distance", distance)
            status = eventObj.optString("status", status)
            eventActivity = eventObj.optString("eventActivity", eventActivity)
            eventDesc = eventObj.optString("eventDesc", eventDesc)
            icon = eventObj.optString("icon", icon)
            topImageId = eventObj.optInt("topImageId", topImageId)
            location = eventObj.optString("location", location)
            voteCount = eventObj.optInt("voteCount", voteCount)
            addDttm = eventObj.optString("addDttm", addDttm)
            lastupdateDttm = eventObj.optString("lastupdateDttm", lastupdateDttm)
            startDttm = eventObj.optString("startDttm", startDttm)
            endDttm = eventObj.optString("endDttm", endDttm)

            // Check for optional "creator" user
            if (eventObj.has("user")) {
                val userObj: JSONObject = eventObj.getJSONObject("user")
                user = UserInfo().setValuesFromResponse(userObj)
            }

            // Check for optional "creator" group
            if (eventObj.has("group")) {
                val groupObj: JSONObject = eventObj.getJSONObject("group")
                group = GroupInfo().setValuesFromResponse(groupObj)
            }

            // Parse comments
            val commentArray: JSONArray = eventObj.getJSONArray("comments")
            comments.clear()
            for (idx in 0 until commentArray.length())
            {
                // Parse comment
                val commentObj = commentArray.getJSONObject(idx)
                val comInfo = CommentInfo().setValuesFromResponse(commentObj)
                comments[comInfo.id] = comInfo
            }

            // Parse user followers
            val userArray: JSONArray = eventObj.getJSONArray("userFollowers")
            userFollowers.clear()
            for (idx in 0 until userArray.length())
            {
                // Parse user
                val userObj = userArray.getJSONObject(idx)
                val userInfo = UserInfo().setValuesFromResponse(userObj)
                userFollowers[userInfo.id] = userInfo
            }

            // Parse group followers
            val groupArray: JSONArray = eventObj.getJSONArray("groupFollowers")
            groupFollowers.clear()
            for (idx in 0 until groupArray.length())
            {
                // Parse group
                val groupObj = groupArray.getJSONObject(idx)
                val groupInfo = GroupInfo().setValuesFromResponse(groupObj)
                groupFollowers[groupInfo.id] = groupInfo
            }
        }
        catch (ex: JSONException)
        {
            ex.printStackTrace()
        }
    }
}
