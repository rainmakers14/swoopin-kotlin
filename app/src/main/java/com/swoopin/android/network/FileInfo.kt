package com.swoopin.android.network

import com.swoopin.android.utility.Utility
import org.json.JSONException
import org.json.JSONObject

// ---------------------------------------------------------------------------------------
//  FileInfo - File info related to a user, a comment, a group, or whatever
// ---------------------------------------------------------------------------------------
class FileInfo
{
    var id = 0
    var hide = 0
    var fileName: String = ""
    var addDttm = ""
    var lastupdateDttm = ""

    init {
        // Set default timestamp
        setTimeStamp()
    }

    // ---------------------------------------------------------------------------------------
    //  setTimeStamp - Set time stamp of object
    // ---------------------------------------------------------------------------------------
    fun setTimeStamp() {
        lastupdateDttm = addDttm
        addDttm = Utility.getTimeStamp(null)
    }

    // ---------------------------------------------------------------------------------------
    //  JSON - Get object as JSON
    // ---------------------------------------------------------------------------------------
    val jSON: JSONObject
        get() {
            setTimeStamp()
            val obj = JSONObject()
            try {
                obj.put("id", id)
                obj.put("hide", hide)
                obj.put("fileName", fileName)
                obj.put("addDttm", addDttm)
                obj.put("lastupdateDttm", lastupdateDttm)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            return obj
        }

    // ---------------------------------------------------------------------------------------
    //  setValuesFromResponse - Set field values from an API response object
    // ---------------------------------------------------------------------------------------
    fun setValuesFromResponse(fileObj: JSONObject): FileInfo
    {
        try
        {
            id = fileObj.optInt("id", id)
            hide = fileObj.optInt("hide", hide)
            fileName = fileObj.optString("fileName", fileName)
            addDttm = fileObj.optString("addDttm", addDttm)
            lastupdateDttm = fileObj.optString("lastupdateDttm", lastupdateDttm)
        }
        catch (ex: JSONException)
        {
            ex.printStackTrace()
        }

        return this
    }
}
