package com.swoopin.android.network

import com.swoopin.android.application.SwoopinApplication
import com.swoopin.android.utility.Settings
import com.swoopin.android.utility.Utility
import org.json.JSONException
import org.json.JSONObject
import java.util.*

// ---------------------------------------------------------------------------------------
//  ProfileInfo - User info. and transformations on that data
// ---------------------------------------------------------------------------------------
class ProfileInfo
{
    var addDttm = ""
    var lastupdateDttm = ""
    var deviceUuid = ""
    var deviceId = 0
    var authToken = ""
    var deviceType = "Device"
    var emailAddr = ""
    var password = ""
    var fileId = 0
    var firstName = ""
    var lastName = ""
    var hideProfile = false
    var id = 0
    var color = 0
    var latitude = "0.0"
    var longitude = "0.0"
    var notificationMask = 1
    var pushToken = ""
    var userType = ""

    init
    {
        // Do this first then fix any missing values
        restoreFromPreferences()

        // Set or create UUID
        val settings = SwoopinApplication.settings()
        if (deviceUuid.isEmpty()) {
            deviceUuid = UUID.randomUUID().toString()
            settings.setStringValue(Settings.PREFKEY_UUID, deviceUuid)
        }

        // Set default timestamp
        setTimeStamp()
    }

    // ---------------------------------------------------------------------------------------
    //  reset - Reset user profile
    // ---------------------------------------------------------------------------------------
    fun reset()
    {
        // Reset all fields
        addDttm = ""
        lastupdateDttm = ""
        deviceUuid = ""
        deviceId = 0
        authToken = ""
        deviceType = "Device"
        emailAddr = ""
        password = ""
        fileId = 0
        firstName = ""
        lastName = ""
        hideProfile = false
        id = 0
        color = 0
        latitude = "0.0"
        longitude = "0.0"
        notificationMask = 1
        pushToken = ""
        userType = ""

        // Save to settings
        saveToPreferences()
    }

    // ---------------------------------------------------------------------------------------
    //  setTimeStamp - Set time stamp of object
    // ---------------------------------------------------------------------------------------
    fun setTimeStamp() {
        lastupdateDttm = addDttm
        addDttm = Utility.getTimeStamp(null)
    }

    // ---------------------------------------------------------------------------------------
    //  JSON - Get object as JSON
    // ---------------------------------------------------------------------------------------
    val jSON: JSONObject
        get() {
            setTimeStamp()
            val obj = JSONObject()
            try {
                obj.put("addDttm", addDttm)
                obj.put("authToken", authToken)
                obj.put("deviceId", deviceId)
                obj.put("deviceType", deviceType)
                obj.put("deviceUuid", deviceUuid)
                obj.put("emailAddr", emailAddr)
                obj.put("fileId", fileId)
                obj.put("firstName", firstName)
                obj.put("hideProfile", hideProfile.toString())
                obj.put("id", id)
                obj.put("color", color)
                obj.put("lastupdateDttm", lastupdateDttm)
                obj.put("latitude", latitude)
                obj.put("longitude", longitude)
                obj.put("notificationMask", notificationMask)
                obj.put("password", password)
                obj.put("pushToken", pushToken)
                obj.put("lastName", lastName)
                obj.put("userType", userType)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            return obj
        }

    // ---------------------------------------------------------------------------------------
    //  saveToPreferences - Save critical data to preferences
    // ---------------------------------------------------------------------------------------
    fun saveToPreferences()
    {
        val settings = SwoopinApplication.settings()
        settings.setIntegerValue(Settings.PREFKEY_USERID, id)
        settings.setIntegerValue(Settings.PREFKEY_FILEID, fileId)
        settings.setStringValue(Settings.PREFKEY_USERTYPE, userType)
        settings.setIntegerValue(Settings.PREFKEY_USERCOLOR, color)
        settings.setStringValue(Settings.PREFKEY_LASTNAME, lastName)
        settings.setStringValue(Settings.PREFKEY_FIRSTNAME, firstName)
        settings.setStringValue(Settings.PREFKEY_EMAIL, emailAddr)
        settings.setStringValue(Settings.PREFKEY_PASSWORD, password)
        settings.setStringValue(Settings.PREFKEY_AUTHTOKEN, authToken)
        settings.setStringValue(Settings.PREFKEY_PUSHTOKEN, pushToken)
        settings.setIntegerValue(Settings.PREFKEY_DEVICEID, deviceId)
        settings.setStringValue(Settings.PREFKEY_DEVICETYPE, deviceType)
        settings.setStringValue(Settings.PREFKEY_UUID, deviceUuid)
        settings.setStringValue(Settings.PREFKEY_LATITUDE, latitude)
        settings.setStringValue(Settings.PREFKEY_LONGITUDE, longitude)
        settings.setBooleanValue(Settings.PREFKEY_HIDEPROFILE, false)
        settings.setIntegerValue(Settings.PREFKEY_NOTIFYMASK, notificationMask)
        settings.setStringValue(Settings.PREFKEY_PREVDATE, lastupdateDttm)
        settings.setStringValue(Settings.PREFKEY_CURDATE, addDttm)
    }

    // ---------------------------------------------------------------------------------------
    //  restoreFromPreferences - Restore critical data from preferences
    // ---------------------------------------------------------------------------------------
    fun restoreFromPreferences()
    {
        val settings = SwoopinApplication.settings()
        id = settings.getIntegerValue(Settings.PREFKEY_USERID, id)
        fileId = settings.getIntegerValue(Settings.PREFKEY_FILEID, fileId)
        color = settings.getIntegerValue(Settings.PREFKEY_USERCOLOR, color)
        lastName = settings.getStringValue(Settings.PREFKEY_LASTNAME, lastName)
        firstName = settings.getStringValue(Settings.PREFKEY_FIRSTNAME, firstName)
        userType = settings.getStringValue(Settings.PREFKEY_USERTYPE, userType)
        emailAddr = settings.getStringValue(Settings.PREFKEY_EMAIL, emailAddr)
        password = settings.getStringValue(Settings.PREFKEY_PASSWORD, password)
        deviceId = settings.getIntegerValue(Settings.PREFKEY_DEVICEID, deviceId)
        deviceType = settings.getStringValue(Settings.PREFKEY_DEVICETYPE, deviceType)
        authToken = settings.getStringValue(Settings.PREFKEY_AUTHTOKEN, authToken)
        pushToken = settings.getStringValue(Settings.PREFKEY_PUSHTOKEN, pushToken)
        deviceUuid = settings.getStringValue(Settings.PREFKEY_UUID, deviceUuid)
        latitude = settings.getStringValue(Settings.PREFKEY_LATITUDE, latitude)
        longitude = settings.getStringValue(Settings.PREFKEY_LONGITUDE, longitude)
        hideProfile = settings.getBooleanValue(Settings.PREFKEY_HIDEPROFILE, hideProfile)
        notificationMask = settings.getIntegerValue(Settings.PREFKEY_NOTIFYMASK, notificationMask)
        lastupdateDttm = settings.getStringValue(Settings.PREFKEY_PREVDATE, lastupdateDttm)
        addDttm = settings.getStringValue(Settings.PREFKEY_CURDATE, addDttm)
    }

    // ---------------------------------------------------------------------------------------
    //  setValuesFromResponse - Set field values from an API response body
    // ---------------------------------------------------------------------------------------
    fun setValuesFromResponse(body: JSONObject)
    {
        try
        {
            val profile = body.getJSONObject("response").getJSONObject("data").getJSONObject("profile")
            val device = profile.getJSONObject("userDevice")
            id = profile.optInt("id", id)
            color = profile.optInt("color", color)
            authToken = device.optString("authToken", authToken)
            pushToken = profile.optString("pushToken", pushToken)
            emailAddr = profile.optString("emailAddr", emailAddr)
            password = profile.optString("password", password)
            firstName = profile.optString("firstName", firstName)
            lastName = profile.optString("lastName", lastName)
            userType = profile.optString("userType", userType)
            deviceType = device.optString("deviceType", deviceType)
            deviceUuid = device.optString("deviceUuid", deviceUuid)
            deviceId = device.optInt("id", deviceId)
            notificationMask = profile.optInt("notificationMask", notificationMask)
            addDttm = profile.optString("addDttm", addDttm)
            lastupdateDttm = profile.optString("lastupdateDttm", lastupdateDttm)
            latitude = profile.optString("latitude", latitude)
            longitude = profile.optString("longitude", longitude)
            hideProfile = profile.optString("hideProfile", hideProfile.toString()).toBoolean()

            // Apparently this is an optional field so treat it accordingly
            if (profile.has("file")) {
                fileId = profile.getJSONObject("file").optInt("id", fileId)
            }
        }
        catch (ex: JSONException)
        {
            ex.printStackTrace()
        }

        // Persist changes
        saveToPreferences()
    }
}
