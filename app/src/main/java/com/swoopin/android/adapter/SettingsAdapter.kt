package com.swoopin.android.adapter

// Android stuff
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.widget.TextView
import android.widget.RadioGroup
import android.view.LayoutInflater
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView

// Swoopin stuff
import com.swoopin.android.application.SwoopinApplication
import com.swoopin.android.utility.Settings
import com.swoopin.android.utility.Utility
import com.swoopin.android.R

// ---------------------------------------------------------------------------------------
//  SettingsAdapter - Adapter for the Settings Activity
// ---------------------------------------------------------------------------------------
class SettingsAdapter(val activity: AppCompatActivity): RecyclerView.Adapter<SettingsAdapter.SettingsViewHolder>()
{
    // Page names
    private val pageName = arrayOf("General", "Privacy Policy", "Terms of Use", "Feedback")
    private val pageResource = arrayOf(R.layout.fragment_settings, R.layout.fragment_privacy, R.layout.fragment_terms, R.layout.fragment_feedback)
    private val pageCount = pageName.size
    private var pageType = 0

    // ---------------------------------------------------------------------------------------
    //  SettingsViewHolder - Holder for all the view items to be filled in by the data
    // ---------------------------------------------------------------------------------------
    inner class SettingsViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)
    {
        // Do nothing here because we don't know the view type?
//        val textView = itemView.findViewById<TextView>(R.id.text_field)
    }

    // ---------------------------------------------------------------------------------------
    //  getItemCount - Get number of pages
    // ---------------------------------------------------------------------------------------
    override fun getItemCount(): Int {
        return pageCount
    }

    // ---------------------------------------------------------------------------------------
    //  getItemViewType - Get View type of specified page
    // ---------------------------------------------------------------------------------------
    override fun getItemViewType(position: Int): Int {
        return position
    }

    // ---------------------------------------------------------------------------------------
    //  onCreateViewHolder - Create the view for the specified page
    // ---------------------------------------------------------------------------------------
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SettingsAdapter.SettingsViewHolder
    {
        // Set so other methods know what type of page is loaded
        pageType = viewType

        // Load specified page type
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        val settingsView = inflater.inflate(pageResource[viewType], parent, false)
        return SettingsViewHolder(settingsView)
    }

    // ---------------------------------------------------------------------------------------
    //  onBindViewHolder - Populate the specified page
    // ---------------------------------------------------------------------------------------
    override fun onBindViewHolder(holder: SettingsViewHolder, position: Int)
    {
        when (position) {
            0 -> bindSettings(holder)
            1 -> bindPrivacy(holder)
            2 -> bindTerms(holder)
            3 -> bindFeedback(holder)
            else -> Utility.logDebug("Tried to bind non-existent Settings page: $position")
        }
    }

    // ---------------------------------------------------------------------------------------
    //  bindSettings - Bind the Settings page
    // ---------------------------------------------------------------------------------------
    private fun bindSettings(holder: SettingsViewHolder)
    {
        // Set maps provider
        val provider = SwoopinApplication.settings().getIntegerValue(Settings.PREFKEY_MAP_PROVIDER, 0)
        val providerGroup = holder.itemView.findViewById<RadioGroup>(R.id.providerGroup)
        if (provider == 0)
            providerGroup.check(R.id.googleButton)
        else
            providerGroup.check(R.id.wazeButton)

        // Set maps provider listener
        providerGroup.setOnCheckedChangeListener { group, checkedId ->
            var newValue = 0
            if (checkedId == R.id.wazeButton)
                newValue = 1
            SwoopinApplication.settings().setIntegerValue(Settings.PREFKEY_MAP_PROVIDER, newValue)
        }

        // Set calendar alert time
        val alertTime = SwoopinApplication.settings().getIntegerValue(Settings.PREFKEY_CALENDAR_ALERT, 0)
        val calendarGroup = holder.itemView.findViewById<RadioGroup>(R.id.calendarGroup)
        when (alertTime) {
            0 -> calendarGroup.check(R.id.noneButton)
            1 -> calendarGroup.check(R.id.min15Button)
            2 -> calendarGroup.check(R.id.min30Button)
            3 -> calendarGroup.check(R.id.hr1Button)
            4 -> calendarGroup.check(R.id.hr2Button)
            else -> Utility.logDebug("Invalid calendar alert value: $alertTime")
        }

        // Set calender alert time listener
        calendarGroup.setOnCheckedChangeListener { group, checkedId ->
            var newValue = 0
            when (checkedId) {
                R.id.noneButton -> newValue = 0
                R.id.min15Button -> newValue = 1
                R.id.min30Button -> newValue = 2
                R.id.hr1Button -> newValue = 3
                R.id.hr2Button -> newValue = 4
                else -> Utility.logDebug("Invalid calendar alert button: $checkedId")
            }
            SwoopinApplication.settings().setIntegerValue(Settings.PREFKEY_CALENDAR_ALERT, newValue)
        }

        // Set version number
        val version = holder.itemView.findViewById<TextView>(R.id.version)
        version.text = String.format(activity.resources.getString(R.string.fmt_version), Utility.getAppVersionName(activity))
    }

    // ---------------------------------------------------------------------------------------
    //  bindPrivacy - Bind the Privacy page
    // ---------------------------------------------------------------------------------------
    private fun bindPrivacy(holder: SettingsViewHolder)
    {
        val webView = holder.itemView.findViewById<WebView>(R.id.web_view)
        webView.loadUrl(activity.resources.getString(R.string.privacy_url))
    }

    // ---------------------------------------------------------------------------------------
    //  bindTerms - Bind the Terms page
    // ---------------------------------------------------------------------------------------
    private fun bindTerms(holder: SettingsViewHolder)
    {
        val webView = holder.itemView.findViewById<WebView>(R.id.web_view)
        webView.loadUrl(activity.resources.getString(R.string.terms_url))
    }

    // ---------------------------------------------------------------------------------------
    //  bindFeedback - Bind the Feedback page
    // ---------------------------------------------------------------------------------------
    private fun bindFeedback(holder: SettingsViewHolder)
    {
        val webView = holder.itemView.findViewById<WebView>(R.id.web_view)
        webView.loadUrl(activity.resources.getString(R.string.feedback_url))
    }
}
