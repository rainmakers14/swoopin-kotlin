package com.swoopin.android.adapter

// Java stuff
import android.annotation.SuppressLint

// Android stuff
import android.view.View
import android.widget.TextView
import android.content.Context
import android.graphics.Bitmap
import android.widget.ImageView
import android.view.LayoutInflater
import androidx.annotation.Nullable
import android.graphics.drawable.Drawable

// Google maps stuff
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter
import com.google.android.gms.maps.model.Marker

// Swoopin stuff
import com.swoopin.android.network.SwoopinAPI
import com.swoopin.android.utility.Utility
import com.swoopin.android.network.Events
import com.swoopin.android.R

// Third party stuff
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition

// ---------------------------------------------------------------------------------------
//  PinMapAdapter - Google Maps Infowindow for a single event pin
// ---------------------------------------------------------------------------------------
class PinMapAdapter(private val context: Context): InfoWindowAdapter
{
    @SuppressLint("InflateParams")
    private val pinWindow = LayoutInflater.from(context).inflate(R.layout.map_info_pin, null)

    // Image caching for async retrieval (necessary because of the way InfoWindow is implemented)
    var marker: Marker? = null
    val eventTarget = Utility.BitmapTarget()
    val creatorTarget = Utility.BitmapTarget()

    // Event creator
    val creatorImg = pinWindow.findViewById<ImageView>(R.id.event_creator)

    // Row 1
    val title = pinWindow.findViewById<TextView>(R.id.event_title)
    val icon = pinWindow.findViewById<ImageView>(R.id.event_icon)
    val startTime = pinWindow.findViewById<TextView>(R.id.event_start)

    // Row 2
    val distance = pinWindow.findViewById<TextView>(R.id.event_distance)
    val numComments = pinWindow.findViewById<TextView>(R.id.num_comments)
    val numAttending = pinWindow.findViewById<TextView>(R.id.num_attending)
    val attendingIcon = pinWindow.findViewById<ImageView>(R.id.attending_icon)

    // Top-voted image
    val eventImg = pinWindow.findViewById<ImageView>(R.id.event_pic)

    // ---------------------------------------------------------------------------------------
    //  getInfoWindow - Get just the containing window?
    // ---------------------------------------------------------------------------------------
    override fun getInfoWindow(marker: Marker): View
    {
        render(marker, pinWindow)
        return pinWindow
    }

    // ---------------------------------------------------------------------------------------
    //  getInfoContents - Get just the window contents?
    // ---------------------------------------------------------------------------------------
    override fun getInfoContents(marker: Marker): View
    {
        render(marker, pinWindow)
        return pinWindow
    }

    // ---------------------------------------------------------------------------------------
    //  render - Called by InfoWindow to do the rendering
    // ---------------------------------------------------------------------------------------
    private fun render(newMarker: Marker, view: View)
    {
        // Reset on new marker
        if (marker != newMarker) {
            marker = newMarker
            eventTarget.bitmap = null
            eventTarget.marker = marker
            creatorTarget.bitmap = null
            creatorTarget.marker = marker
        }

        // Extract event ID from marker
        val evId: Int = marker?.tag as Int

        // Set text fields
        title.text = Events.getTitle(evId)
        startTime.text = Events.timeToEvent(evId)
        distance.text = Events.formatDistance(evId)
        numComments.text = Events.numComments(evId).toString()
        numAttending.text = Events.numAttending(evId).toString()

        // Set event icon
        icon.setImageBitmap(Events.getEventIcon(evId))

        // Set attending icon
        attendingIcon.visibility = View.INVISIBLE
        if (Events.isAttending(SwoopinAPI.profileInfo.id, evId))
            attendingIcon.visibility = View.VISIBLE

        // Set creator icon
        val creatorImageFileName = Events.getCreatorImage(evId)
        creatorImg.visibility = View.VISIBLE
        if (creatorImageFileName.isEmpty()) {
            // Nothing to display
            creatorImg.visibility = View.GONE
        }
        else if (creatorTarget.bitmap != null) {
            // Image has been downloaded so display it
            creatorImg.setImageBitmap(creatorTarget.bitmap)
        }
        else {
            // Download image in background
            SwoopinAPI.loadCircularBitmap(creatorImageFileName, creatorTarget)
        }

        // Set event image
        val topImageFileName = Events.getTopImage(evId)
        eventImg.visibility = View.VISIBLE
        if (topImageFileName.isEmpty()) {
            // Nothing to display
            eventImg.visibility = View.GONE
        }
        else if (eventTarget.bitmap != null) {
            // Image has been downloaded so display it
            eventImg.setImageBitmap(eventTarget.bitmap)
        }
        else {
            // Download image in background
            SwoopinAPI.loadBitmap(topImageFileName, eventTarget)
        }
    }
}
