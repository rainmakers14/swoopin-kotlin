package com.swoopin.android.adapter

// Android stuff
import android.view.View
import android.view.ViewGroup
import android.content.Context
import android.widget.TextView
import android.widget.ImageView
import android.view.LayoutInflater
import androidx.recyclerview.widget.RecyclerView
import androidx.appcompat.widget.AppCompatImageButton

// Swoopin stuff
import com.swoopin.android.R
import com.swoopin.android.network.Users
import com.swoopin.android.network.Events
import com.swoopin.android.network.SwoopinAPI
import com.swoopin.android.network.CommentInfo
import com.swoopin.android.application.SwoopinApplication

// ---------------------------------------------------------------------------------------
//  CommentListAdapter - Adapter for comment list item
// ---------------------------------------------------------------------------------------
class CommentListAdapter constructor(private val context: Context): RecyclerView.Adapter<CommentListAdapter.ViewHolder>()
{
    // List of comments
    private var commentList = mutableListOf<CommentInfo>()

    // ---------------------------------------------------------------------------------------
    //  ViewHolder - Holder for all the view items to be filled in by the data
    // ---------------------------------------------------------------------------------------
    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)
    {
        val authorName = itemView.findViewById<TextView>(R.id.author_name)
        val authorIcon = itemView.findViewById<ImageView>(R.id.author_icon)
        val commentText = itemView.findViewById<TextView>(R.id.comment_text)
        val commentImage = itemView.findViewById<ImageView>(R.id.comment_pic)
        val commentTime = itemView.findViewById<TextView>(R.id.comment_time)
        val voteCount = itemView.findViewById<TextView>(R.id.vote_count)
        val voteIcon: AppCompatImageButton = itemView.findViewById(R.id.vote_icon)
    }

    // ---------------------------------------------------------------------------------------
    //  getCommentId - Get comment ID from list index
    // ---------------------------------------------------------------------------------------
    fun getCommentId(index: Int): Int {
        return commentList[index].id
    }

    // ---------------------------------------------------------------------------------------
    //  getComment - Get comment at specified index
    // ---------------------------------------------------------------------------------------
    fun getComment(index: Int): CommentInfo {
        return commentList[index]
    }

    // ---------------------------------------------------------------------------------------
    //  setList - Set updated list of comments
    // ---------------------------------------------------------------------------------------
    fun setList(newList: MutableList<CommentInfo>)
    {
        // Remove old list
        val oldSize = commentList.size
        commentList.clear()
        notifyItemRangeRemoved(0, oldSize)

        // Add new list
        commentList.addAll(newList)
        notifyItemRangeInserted(0, commentList.size)
    }

    // ---------------------------------------------------------------------------------------
    //  getItemCount - Return total number of items in the list
    // ---------------------------------------------------------------------------------------
    override fun getItemCount(): Int {
        return commentList.size
    }

    // ---------------------------------------------------------------------------------------
    //  onCreateViewHolder - Create the view for one comment item
    // ---------------------------------------------------------------------------------------
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentListAdapter.ViewHolder
    {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        val commentItemView = inflater.inflate(R.layout.comment_list_item, parent, false)
        return ViewHolder(commentItemView)
    }

    // ---------------------------------------------------------------------------------------
    //  onBindViewHolder - Populate the specified comment item view
    // ---------------------------------------------------------------------------------------
    override fun onBindViewHolder(viewHolder: CommentListAdapter.ViewHolder, position: Int)
    {
        // Get the data model based on position
        val comment: CommentInfo = commentList[position]

        // Set author name
        val audthorId = Users.userMap[comment.userId]
        viewHolder.authorName.text = Events.getCommentAuthorName(comment)

        // Set author icon
        val authorIconFileName = Events.getCommentAuthorIcon(comment)
        if (authorIconFileName.isEmpty()) {
            viewHolder.authorIcon.visibility = View.GONE
        }
        else {
            viewHolder.authorIcon.visibility = View.VISIBLE
            SwoopinAPI.loadImage(authorIconFileName, viewHolder.authorIcon)
        }

        // Set comment info
        viewHolder.commentText.text = comment.comment
        val commentImageFileName = Events.getCommentImage(comment)
        if (commentImageFileName.isEmpty()) {
            viewHolder.commentImage.visibility = View.GONE
        }
        else {
            viewHolder.commentImage.visibility = View.VISIBLE
            SwoopinAPI.loadImage(commentImageFileName, viewHolder.commentImage)
        }

        // Set vote count
        viewHolder.voteCount.text = comment.voteCount.toString()

        // Remember ID so the Activity can increment the vote count
        viewHolder.voteIcon.tag = position

        // Set vote icon based on user's vote
        if (comment.userVoted > 0)
            viewHolder.voteIcon.setImageResource(R.drawable.ic_fire_48)
        else
            viewHolder.voteIcon.setImageResource(R.drawable.ic_vote_48_outline)

        // Set comment time
        viewHolder.commentTime.text = String.format(SwoopinApplication.instance.getString(R.string.fmt_time_ago), Events.timeSinceComment(comment))
    }
}
