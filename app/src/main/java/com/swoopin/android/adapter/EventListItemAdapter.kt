package com.swoopin.android.adapter

// Java stuff
import java.time.Duration
import java.time.ZoneOffset
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import android.content.res.Resources

// Android stuff
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.ImageView
import android.view.LayoutInflater
import androidx.recyclerview.widget.RecyclerView

// Swoopin stuff
import com.swoopin.android.R
import com.swoopin.android.network.EventInfo
import com.swoopin.android.network.SwoopinAPI
import com.swoopin.android.application.SwoopinApplication
import com.swoopin.android.network.Events
import com.swoopin.android.utility.Utility

// ---------------------------------------------------------------------------------------
//  EventListItemAdapter - Adapter for event list view
// ---------------------------------------------------------------------------------------
class EventListItemAdapter constructor(val clickListener: EventItemClickListener): RecyclerView.Adapter<EventListItemAdapter.ViewHolder>()
{
    // Use to subscribe to event item clicks
    interface EventItemClickListener {
        fun onEventClicked(eventIndex: Int)
        fun onEventLongPressed(eventIndex: Int)
    }

    // List of events
    private var eventList = mutableListOf<EventInfo>()

    // ---------------------------------------------------------------------------------------
    //  getEventId - Get event ID from list index
    // ---------------------------------------------------------------------------------------
    fun getEventId(index: Int): Int {
        return eventList[index].id
    }

    // ---------------------------------------------------------------------------------------
    //  setList - Set updated list of events
    // ---------------------------------------------------------------------------------------
    fun setList(newList: MutableList<EventInfo>)
    {
        // Remove old list
        val oldSize = eventList.size
        eventList.clear()
        notifyItemRangeRemoved(0, oldSize)

        // Add new list
        eventList.addAll(newList)
        notifyItemRangeInserted(0, eventList.size)
    }

    // ---------------------------------------------------------------------------------------
    //  clear - Clear the list of events
    // ---------------------------------------------------------------------------------------
    fun clear()
    {
        val oldSize = eventList.size
        eventList.clear()
        notifyItemRangeRemoved(0, oldSize)
    }

    // ---------------------------------------------------------------------------------------
    //  ViewHolder - Holder for all the view items to be filled in by the data
    // ---------------------------------------------------------------------------------------
    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)
    {
        val title = itemView.findViewById<TextView>(R.id.event_title)
        val description = itemView.findViewById<TextView>(R.id.event_desc)
        val creator = itemView.findViewById<ImageView>(R.id.event_creator)
        val eventPic = itemView.findViewById<ImageView>(R.id.event_pic)
        val eventIcon = itemView.findViewById<ImageView>(R.id.event_icon)
        val distance = itemView.findViewById<TextView>(R.id.event_distance)
        val numComments = itemView.findViewById<TextView>(R.id.num_comments)
        val numAttending = itemView.findViewById<TextView>(R.id.num_attending)
        val eventStart = itemView.findViewById<TextView>(R.id.event_start)

        init {
            itemView.setOnClickListener{ clickListener.onEventClicked(adapterPosition) }
            itemView.setOnLongClickListener{ clickListener.onEventLongPressed(adapterPosition)
                return@setOnLongClickListener true }
        }
    }

    // ---------------------------------------------------------------------------------------
    //  getItemCount - Return total number of items in the event list
    // ---------------------------------------------------------------------------------------
    override fun getItemCount(): Int {
        return eventList.size
    }

    // ---------------------------------------------------------------------------------------
    //  onCreateViewHolder - Create the view for one event item
    // ---------------------------------------------------------------------------------------
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventListItemAdapter.ViewHolder
    {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        val eventItemView = inflater.inflate(R.layout.event_list_item, parent, false)
        return ViewHolder(eventItemView)
    }

    // ---------------------------------------------------------------------------------------
    //  onBindViewHolder - Populate the specified event item view
    // ---------------------------------------------------------------------------------------
    override fun onBindViewHolder(viewHolder: EventListItemAdapter.ViewHolder, position: Int)
    {
        // Get the data model based on position
        val event: EventInfo = eventList[position]

        // Set text items
        viewHolder.title.text = event.eventActivity
        viewHolder.description.text = event.eventDesc
        viewHolder.distance.text = Events.formatDistance(event.id)

        viewHolder.eventStart.text = Events.timeToEvent(event.id)
        viewHolder.numComments.text = event.comments.size.toString()
        viewHolder.numAttending.text = event.userFollowers.size.toString()

        // Set event icon
        viewHolder.eventIcon.setImageBitmap(Events.getEventIcon(event.id))

        // Set event picture
        val topImageFileName = Events.getTopImage(event.id)
        if (topImageFileName.isEmpty()) {
            viewHolder.eventPic.visibility = View.GONE
        }
        else {
            viewHolder.eventPic.visibility = View.VISIBLE
            SwoopinAPI.loadImage(topImageFileName, viewHolder.eventPic)
        }

        // Set creator icon
        val creatorImageFileName = Events.getCreatorImage(event.id)
        if (creatorImageFileName.isEmpty()) {
            viewHolder.creator.visibility = View.GONE
        }
        else {
            viewHolder.creator.visibility = View.VISIBLE
            SwoopinAPI.loadImage(creatorImageFileName, viewHolder.creator)
        }
    }
}
