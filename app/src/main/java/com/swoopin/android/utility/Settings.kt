package com.swoopin.android.utility

import android.content.Context

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.*
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.preferencesDataStore

import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.runBlocking
import java.io.IOException

class Settings(val appContext: Context)
{
    companion object
    {
        // Log tag for errors
        const val LOGTAG = "Settings"

        // Preference keys
        const val PREFKEY_GCM_ID = "CGMID"
        const val PREFKEY_MAPMODE = "MAPMODE"
        const val PREFKEY_MAPSTYLE = "MAPSTYLE"
        const val PREFKEY_MAP_PROVIDER = "MAPPROVIDER"
        const val PREFKEY_CALENDAR_ALERT = "CALENDARALERT"
        const val PREFKEY_PROFILE_PIC = "PROFILEPIC"
        const val PREFKEY_TERMS_ACCEPTED = "TERMS"

        // Profile Info
        const val PREFKEY_UUID = "UUID"
        const val PREFKEY_FILEID = "FILEID"
        const val PREFKEY_EMAIL = "EMAIL"
        const val PREFKEY_USERID = "USERID"
        const val PREFKEY_LASTNAME = "LASTNAME"
        const val PREFKEY_FIRSTNAME = "FIRSTNAME"
        const val PREFKEY_USERTYPE = "USERTYPE"
        const val PREFKEY_PASSWORD = "PASSWORD"
        const val PREFKEY_USERCOLOR = "USERCOLOR"
        const val PREFKEY_DEVICEID = "DEVICEID"
        const val PREFKEY_DEVICETYPE = "DEVICETYPE"
        const val PREFKEY_LATITUDE = "LATITUDE"
        const val PREFKEY_LONGITUDE = "LONGITUDE"
        const val PREFKEY_AUTHTOKEN = "AUTHTOKEN"
        const val PREFKEY_PUSHTOKEN = "PUSHTOKEN"
        const val PREFKEY_NOTIFYMASK = "NOTIFYMASK"
        const val PREFKEY_HIDEPROFILE = "HIDEPROFILE"
        const val PREFKEY_PREVDATE = "PREVDATE"
        const val PREFKEY_CURDATE = "CURDATE"
    }

    // Create data store
    private val context = appContext
    private val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = "settings")

    // Delete all settings data
    fun clear() {
        runBlocking {
            context.dataStore.edit { it.clear() }
        }
    }

    // ----------------------------------------------------------------------
    //  String Values
    // ----------------------------------------------------------------------

    // Set value of an arbitrary key
    fun setStringValue(keyStr: String, valueStr: String) {
        val key = stringPreferencesKey(keyStr)
        runBlocking {
            context.dataStore.edit { preferences -> preferences[key] = valueStr }
        }
    }

    // Get value of an arbitrary key
    fun getStringValue(keyStr: String, defaultValue: String): String
    {
        val key = stringPreferencesKey(keyStr)
        val value = runBlocking {
            context.dataStore.data.map { it[key] ?: defaultValue }
        }
        .catch { exception ->
            if (exception is IOException)
                Utility.logError(LOGTAG, exception.message.toString())
            else
                throw exception
        }

        return runBlocking { value.first() }
    }

    // ----------------------------------------------------------------------
    //  Boolean Values
    // ----------------------------------------------------------------------

    // Set value of an arbitrary key
    fun setBooleanValue(keyStr: String, value: Boolean) {
        val key = booleanPreferencesKey(keyStr)
        runBlocking {
            context.dataStore.edit { preferences -> preferences[key] = value }
        }
    }

    // Get value of an arbitrary key
    fun getBooleanValue(keyStr: String, defaultValue: Boolean): Boolean
    {
        val key = booleanPreferencesKey(keyStr)
        val value = runBlocking {
            context.dataStore.data.map { it[key] ?: defaultValue }
        }
            .catch { exception ->
                if (exception is IOException)
                    Utility.logError(LOGTAG, exception.message.toString())
                else
                    throw exception
            }

        return runBlocking { value.first() }
    }

    // ----------------------------------------------------------------------
    //  Integer Values
    // ----------------------------------------------------------------------

    // Set value of an arbitrary key
    fun setIntegerValue(keyStr: String, value: Int) {
        val key = intPreferencesKey(keyStr)
        runBlocking {
            context.dataStore.edit { preferences -> preferences[key] = value }
        }
    }

    // Get value of an arbitrary key
    fun getIntegerValue(keyStr: String, defaultValue: Int): Int
    {
        val key = intPreferencesKey(keyStr)
        val value = runBlocking {
            context.dataStore.data.map { it[key] ?: defaultValue }
        }
            .catch { exception ->
                if (exception is IOException)
                    Utility.logError(LOGTAG, exception.message.toString())
                else
                    throw exception
            }

        return runBlocking { value.first() }
    }
}
