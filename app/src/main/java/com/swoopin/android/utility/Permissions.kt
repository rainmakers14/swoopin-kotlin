@file:Suppress("PrivatePropertyName", "SpellCheckingInspection", "LocalVariableName")

package com.swoopin.android.utility

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import androidx.core.app.ActivityCompat
import android.content.pm.PackageManager
import androidx.core.content.ContextCompat

import android.util.Log
import android.widget.Toast
import com.google.android.gms.location.LocationRequest
import com.swoopin.android.R

// Android documentation on permissions
// https://developer.android.com/training/permissions/requesting
// https://github.com/VMadalin/easypermissions-ktx

// New way to do Location is using Google Play Services
// https://developer.android.com/training/location/request-updates
// https://developer.android.com/training/location/retrieve-current
// https://developers.google.com/android/reference/com/google/android/gms/location/FusedLocationProviderClient
// https://stackoverflow.com/questions/45958226/get-location-android-kotlin

// Handles the details of permission checking
class Permissions(owner: Activity)
{
    // Context for checking permissions
    private var activity = owner

    // Log tag for this object
    private val LOGTAG = "PERMISSIONS"

    // Request code for permissions
    private val REQCODE_PERMISSION = 101

    // Location request parameters
    val eventLocationRequest = LocationRequest.create().apply {
        interval = 30000             // Update every 30 seconds
        fastestInterval = 10000
        smallestDisplacement = 100f  // 100m = 0.06 of a mile
        maxWaitTime = 30000
        priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    // Verify camera permission
    fun hasCameraPermission(): Boolean {
        return isPermissionsGranted(Manifest.permission.CAMERA)
    }

    // Check camera permission
    fun checkCameraPermission() {
        val permList = arrayOf(Manifest.permission.CAMERA)
        if (!isPermissionsGranted(permList[0]))
            getPermission(permList, activity.getString(R.string.camera_rationale))
    }

    // Verify external storage permission
    fun hasFilePermission(): Boolean {
        return isPermissionsGranted(Manifest.permission.READ_EXTERNAL_STORAGE)
    }

    // Check external storage permission
    // If we're doing this just for images and other media, we need to use the new way
    // https://developer.android.com/training/data-storage/shared/media
    fun checkFilePermission() {
        val permList = arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE)
        if (!isPermissionsGranted(permList[0]))
            getPermission(permList, activity.getString(R.string.storage_rationale))
    }

    // Verify contacts permission
    fun hasContactsPermission(): Boolean {
        return isPermissionsGranted(Manifest.permission.READ_CONTACTS)
    }

    // Check contacts permission
    fun checkContactsPermission() {
        val permList = arrayOf(Manifest.permission.READ_CONTACTS)
        if (!isPermissionsGranted(permList[0]))
            getPermission(permList, activity.getString(R.string.location_rationale))
    }

    // Verify location permission
    fun hasLocationPermission(): Boolean {
        return isPermissionsGranted(Manifest.permission.ACCESS_FINE_LOCATION)
    }

    // Check location permission
    fun checkLocationPermission() {
        val permList = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
        if (!isPermissionsGranted(permList[0]))
            getPermission(permList, activity.getString(R.string.location_rationale))
    }

    // Check permission status
    private fun isPermissionsGranted(permList: String): Boolean {
        return (ContextCompat.checkSelfPermission(activity, permList) == PackageManager.PERMISSION_GRANTED)
    }

    // Try to get a permission
    fun getPermission(permList: Array<String>, rationale: String)
    {
        // No need to do anything if we already have the permission
        if (!isPermissionsGranted(permList[0]))
        {
            // Requests after the first one must give a rationale to the user
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permList[0]))
                showRationale(permList, rationale)
            else
                requestPermission(permList)
        }
    }

    // Request specified permissions
    private fun requestPermission(permList: Array<String>) {
        ActivityCompat.requestPermissions(activity, permList, REQCODE_PERMISSION)
    }

    // Show rationale via alert dialog
    private fun showRationale(permList: Array<String>, message: String)
    {
        val builder = AlertDialog.Builder(activity)
        builder.setTitle("Permission Required")
        builder.setMessage(message)
        builder.setPositiveButton("OK") { _, _ -> requestPermission(permList) }
//        builder.setNeutralButton("Cancel", null)
        val dialog = builder.create()
        dialog.show()
    }

    // Process permissions result
    fun onRequestPermissionsResult(requestCode: Int, permList: Array<String>, grantResults: IntArray)
    {
        if ((requestCode == REQCODE_PERMISSION) && permList.isNotEmpty() && grantResults.isNotEmpty())
        {
            val granted = (grantResults[0] == PackageManager.PERMISSION_GRANTED)
            val logMsg = StringBuilder().append("Permission ").append(if (granted) "granted: " else "denied: ").append(permList[0]).toString()
            Toast.makeText(activity, logMsg, Toast.LENGTH_SHORT).show()
            Log.i(LOGTAG, logMsg)
        }
    }
}
