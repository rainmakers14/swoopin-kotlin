@file:Suppress("PrivatePropertyName", "SpellCheckingInspection")

package com.swoopin.android.utility

// Java stuff
import java.time.format.DateTimeFormatter
import java.text.SimpleDateFormat
import java.util.*

// Android stuff
import android.hardware.camera2.CameraManager
import android.graphics.drawable.Drawable
import android.content.pm.PackageManager
import androidx.annotation.Nullable
import android.app.ActivityManager
import android.app.Application
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.content.Intent
import android.graphics.Color
import android.util.Base64
import android.util.Log
import android.net.Uri

// Swoopin stuff
import com.swoopin.android.network.SwoopinAPI
import com.swoopin.android.BuildConfig

// Third party stuff
import com.bumptech.glide.request.transition.Transition
import com.bumptech.glide.request.target.CustomTarget
import com.amulyakhare.textdrawable.TextDrawable
import com.google.android.gms.maps.model.Marker
import java.io.ByteArrayOutputStream
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.time.Duration

// Static object singleton
object Utility
{
    val LOGTAG = "Utility"
    val BITMAP_SCALE = 0.6F

    // Determine if device has a camera
    var hasCamera: Boolean? = null

    // ---------------------------------------------------------------------------------------
    //  hasCamera - Determine if device has a camera
    // ---------------------------------------------------------------------------------------
    fun hasCamera(context: Context): Boolean
    {
        // Only do the work once
        if (hasCamera == null)
        {
            // Default value
            hasCamera = context.packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY)

            // Secondary check because some older devices lie about having a camera
            val cameraManager = context.getSystemService(Context.CAMERA_SERVICE) as CameraManager
            val numCameras: Int = cameraManager.cameraIdList.size
            if (numCameras < 1)
                hasCamera = false
        }

        return hasCamera as Boolean
    }

    // ---------------------------------------------------------------------------------------
    //  openURL - Open specified URL using system browser
    // ---------------------------------------------------------------------------------------
    fun openURL(context: Context?, address: String)
    {
        val url = Uri.parse(address)
        val newIntent = Intent(Intent.ACTION_VIEW, url)
        context!!.startActivity(newIntent)
    }

    // ---------------------------------------------------------------------------------------
    //  logDebug - Conditional logging with a custom log tag
    // ---------------------------------------------------------------------------------------
    fun logDebug(logtag: String, message: String) {
        if (BuildConfig.DEBUG)
            Log.d(logtag, message)
    }

    // ---------------------------------------------------------------------------------------
    //  logDebug - Conditional logging without a tag
    // ---------------------------------------------------------------------------------------
    fun logDebug(message: String) {
        if (BuildConfig.DEBUG)
            Log.d(LOGTAG, message)
    }

    // ---------------------------------------------------------------------------------------
    //  logError - Unconditional logging with a custom log tag
    // ---------------------------------------------------------------------------------------
    fun logError(logtag: String, message: String) {
        Log.d(logtag, message)
    }

    // ---------------------------------------------------------------------------------------
    //  logError - Unconditional logging without a tag
    // ---------------------------------------------------------------------------------------
    fun logError(message: String) {
        Log.d(LOGTAG, message)
    }

    // ---------------------------------------------------------------------------------------
    //  logMemoryMetrics - Print memory metrix of device
    // ---------------------------------------------------------------------------------------
    fun logMemoryMetrics(context: Context)
    {
        // Get memory info.
        val memInfo = ActivityManager.MemoryInfo()
        val activityManager = context.getSystemService(Application.ACTIVITY_SERVICE) as ActivityManager
        activityManager.getMemoryInfo(memInfo)

        // https://developer.android.com/reference/android/app/ActivityManager.MemoryInfo
        // 'availMem' - The available memory on the system. This number should not be considered absolute: due to the nature of the kernel,
        // a significant portion of this memory is actually in use and needed for the overall system to run well

        // Calculate available memory
        val ONE_GIG = 1024 * 1024 * 1024.toDouble()
        val memUsed = ((memInfo.totalMem - memInfo.availMem) / ONE_GIG)
        val memTotal = (memInfo.totalMem / ONE_GIG)
        val memFree = memInfo.availMem / ONE_GIG
        val threshold = (memInfo.threshold / ONE_GIG)

        logDebug(LOGTAG,"+----------------------------DEVICE MEMORY INFO ---------")
        logDebug(LOGTAG,"| Total memory: " + String.format("%.2f", memTotal))
        logDebug(LOGTAG,"| Free memory:  " + String.format("%.2f", memFree))
        logDebug(LOGTAG,"| Used memory:  " + String.format("%.2f", memUsed))
        logDebug(LOGTAG,"| Threshold: " + String.format("%.2f", threshold))
        logDebug(LOGTAG,"| Memory currently low: " + memInfo.lowMemory)
        logDebug(LOGTAG,"+--------------------------------------------------------")
    }

    // ---------------------------------------------------------------------------------------
    //  getNow - Get the LocalDateTime of now
    // ---------------------------------------------------------------------------------------
    fun getNow(): LocalDateTime
    {
        return LocalDateTime.now(ZoneOffset.UTC)
    }

    // ---------------------------------------------------------------------------------------
    //  getLocalDateTime - Get the LocalDateTime of the formatted string time
    // ---------------------------------------------------------------------------------------
    fun getLocalDateTime(dateStr: String): LocalDateTime
    {
        val formatter = DateTimeFormatter.ofPattern(SwoopinAPI.DATE_FORMAT).withZone(ZoneOffset.UTC).withLocale(Locale.getDefault())
        return formatter.parse(dateStr, LocalDateTime::from)
    }

    // ---------------------------------------------------------------------------------------
    //  getTimeStamp - Get a properly formatted timestampe for "now"
    // ---------------------------------------------------------------------------------------
    fun getTimeStamp(ldt: LocalDateTime?): String
    {
        // Use "now" by default
        var time = LocalDateTime.now(ZoneOffset.UTC)
        if (ldt != null)
            time = ldt

        // Format time string
        val formatter = DateTimeFormatter.ofPattern(SwoopinAPI.DATE_FORMAT).withZone(ZoneOffset.UTC).withLocale(Locale.getDefault())
        return formatter.format(time)

//        val formatter = SimpleDateFormat(SwoopinAPI.DATE_FORMAT, Locale.getDefault())
//        formatter.timeZone = TimeZone.getTimeZone("UTC")
//        return formatter.format(Date())
    }

    // ---------------------------------------------------------------------------------------
    //  getTimeSince - Get the duration between now and a time in the past
    // ---------------------------------------------------------------------------------------
    fun getTimeSince(dateStr: String): Duration
    {
        val formatter = DateTimeFormatter.ofPattern(SwoopinAPI.DATE_FORMAT).withZone(ZoneOffset.UTC).withLocale(Locale.getDefault())
        val pastDate: LocalDateTime = formatter.parse(dateStr, LocalDateTime::from)
        return Duration.between(pastDate, LocalDateTime.now(ZoneOffset.UTC))
    }

    // ---------------------------------------------------------------------------------------
    //  getTimeUntil - Get the duration between now and a time in the future
    // ---------------------------------------------------------------------------------------
    fun getTimeUntil(dateStr: String): Duration
    {
        val formatter = DateTimeFormatter.ofPattern(SwoopinAPI.DATE_FORMAT).withZone(ZoneOffset.UTC).withLocale(Locale.getDefault())
        val futureDate: LocalDateTime = formatter.parse(dateStr, LocalDateTime::from)
        return Duration.between(LocalDateTime.now(ZoneOffset.UTC), futureDate)
    }

    // ---------------------------------------------------------------------------------------
    //  getAppVersionName - Get the app version name
    // ---------------------------------------------------------------------------------------
    fun getAppVersionName(context: Context): String
    {
        var version = ""
        try {
            val pInfo = context.packageManager.getPackageInfo(context.packageName, 0)
            version = pInfo.versionName
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }

        return version
    }

    // ---------------------------------------------------------------------------------------
    //  BitmapTarget - Off-screen targte for image loading
    // ---------------------------------------------------------------------------------------
    class BitmapTarget() : CustomTarget<Bitmap>()
    {
        var bitmap: Bitmap? = null
        var marker: Marker? = null

        override fun onLoadCleared(@Nullable placeholder: Drawable?) {
            bitmap = null
        }

        override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
            bitmap = resource
            marker?.showInfoWindow()
        }
    }

    // ---------------------------------------------------------------------------------------
    //  getEmojiBitmap - Convert emoji character to a bitmap
    //     https://jitpack.io/p/alvinhkh/TextDrawable
    // ---------------------------------------------------------------------------------------
    fun getEmojiBitmap(emoji: String, ctx: Context): Bitmap
    {
        // Create drawable
        val shape: Drawable = TextDrawable.Builder()
            .setShape(TextDrawable.SHAPE_ROUND)
            .setBorderColor(Color.BLUE)
            .setColor(Color.TRANSPARENT)
            .setText(emoji)
            .build()

        // Set dimensions based on screen density
        val dp = (48 * ctx.resources.displayMetrics.density).toInt()

        // Create the bitmap
        val emojiBitmap = Bitmap.createBitmap(dp, dp, Bitmap.Config.ARGB_8888)

        // Set bounding box for shape
        val canvas = Canvas(emojiBitmap)
        shape.setBounds(0, 0, emojiBitmap.width, emojiBitmap.height)

        // Draw shape
        shape.draw(canvas)

        // Return constructed bitmap
        return emojiBitmap
    }

    // ---------------------------------------------------------------------------------------
    //  ScaleBitmapForServer - Scale bitmap for server storage
    // ---------------------------------------------------------------------------------------
    fun scaleBitmapForServer(inBitmap: Bitmap) : Bitmap
    {
        // Create scaled bitmap
        val scaledWidth = inBitmap.width * BITMAP_SCALE
        val scaledHeight = inBitmap.height * BITMAP_SCALE
        return Bitmap.createScaledBitmap(inBitmap, scaledWidth.toInt(), scaledHeight.toInt(), false)
    }

    // ---------------------------------------------------------------------------------------
    //  bitmapToJpg - Convert bitmap to JPG format
    // ---------------------------------------------------------------------------------------
    fun bitmapToJpg(bitmap: Bitmap) : ByteArray
    {
        // Convert bitmap to byte array
        val stream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream)
        return stream.toByteArray()
    }

    // ---------------------------------------------------------------------------------------
    //  bitmapToBase64 - Convert bitmap to Base64 string
    // ---------------------------------------------------------------------------------------
    fun bitmapToBase64(bitmap: Bitmap) : String
    {
        // Convert bitmap to byte array
        val stream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream)

        // Convert byte arrat to string
        return Base64.encodeToString(stream.toByteArray(), Base64.NO_WRAP)
    }
}
