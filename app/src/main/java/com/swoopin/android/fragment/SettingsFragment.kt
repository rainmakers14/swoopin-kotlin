@file:Suppress("PrivatePropertyName", "SpellCheckingInspection", "SpellCheckingInspection")

package com.swoopin.android.fragment

// Android stuff
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.content.Context
import android.view.LayoutInflater
import androidx.fragment.app.Fragment
import androidx.appcompat.app.AppCompatActivity

// Swoopin stuff
import com.swoopin.android.R
import com.swoopin.android.utility.Utility
import com.swoopin.android.utility.Settings
import com.swoopin.android.application.SwoopinApplication
import com.swoopin.android.databinding.ActivityAcceptTermsBinding
import com.swoopin.android.utility.Settings.Companion.PREFKEY_TERMS_ACCEPTED

// ---------------------------------------------------------------------------------------
// SettingsGeneralFragment - First page of Settings Page View
// ---------------------------------------------------------------------------------------
class SettingsGeneralFragment : Fragment()
{
    // ---------------------------------------------------------------------------------------
    // onCreateView - Create the fragment view
    // ---------------------------------------------------------------------------------------
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_settings, container, false)
    }

}
